 #
 # Bruviti Confidential
 #
 # Copyright © 2019 Bruviti. All rights reserved.
 # 
 # Notice: All information contained herein is, and remains the property of Bruviti
 # and its suppliers, if any. The intellectual and technical concepts
 # contained herein are proprietary to Bruviti and/or its suppliers, and may be
 # covered by U.S. and foreign patents or patents in process, and are protected
 # by trade secret or copyright law. Dissemination of this information or
 # reproduction of this material is strictly forbidden unless prior written
 # permission is obtained from Bruviti.
 #
 # docker build -t <tag-name> --no-cache .
 # docker run -p 8888:8080-e API_URL='http://geadevtestvm.bruviti.com:5052' -e BOT_URL='http://geadevtestvm.bruviti.com:5050' -e VAD_URL='http://geadevtestvm.bruviti.com:9090' <tag-name>
 # Eg port on host machine 8888 is mapped to 8080 in the container
 # 

FROM bruvitidocker/ps:ub18node11

LABEL vendor="Bruviti Inc" \
    maintainer="Bruviti Inc" \
    version="1.0" \
    name="Bruviti VA-Dashboard" \
    description="Bruviti's VA-Dashboard Image"

####################### Create non-root user ########################
RUN useradd -ms /bin/bash  ubuntu
RUN useradd -ms /bin/bash  docker
RUN chown -R ubuntu.ubuntu /home/ubuntu
RUN echo root:BPs24!9! | /usr/sbin/chpasswd
RUN echo ubuntu:brpwd | /usr/sbin/chpasswd
#####################################################################

USER root
ENV API_URL="http://localhost:8080" \
    BOT_URL="https://geadevtestvm.bruviti.com:5050"\
    VAD_URL=""

#####################################################################
RUN mkdir va-dashboard
#COPY build /va-dashboard/build
COPY build /va-dashboard/public
COPY deploy/* /va-dashboard/
####################################################################

# Create app directory
WORKDIR /va-dashboard

# Install app dependencies
RUN npm install

RUN chown -R ubuntu:ubuntu /va-dashboard

EXPOSE 9090

USER ubuntu
ENV HOME /home/ubuntu

RUN npm --version

CMD [ "node", "server.js"]
