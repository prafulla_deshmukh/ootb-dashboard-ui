import * as React from "react";
import * as ReactDOM from "react-dom";
import "./app.scss";
import Config from "./config";
import routes from "./routes";
import logo from "./images/gea_haier_logo.png";

import { BrowserRouter as Router, Switch } from "react-router-dom";
import { PrivateRoute } from "./modules/auth/PrivateRoute";
import * as _ from "lodash";

import { Service as AuthService } from "./modules/auth";
import { LoginRoute } from "./modules/auth/LoginRoute";

import ApiClient from "./lib/api-client";
import * as Util from "./lib/util";

import { session as SessionStore } from "store2";
import { FeedBackDrawer } from "./modules/feedback";
import Service from "./Service";
import { withRouter } from "react-router";

const Api = ApiClient.instance;

if (Util.IE.isTheBrowser && Util.IE.actualVersion === "10") {
  import("../shim_lib/es5-shim.min.js");
  import("../shim_lib/es6-shim.min.js");
  import("../shim_lib/es7-shim@latest.js");
  import("../shim_lib/html5shiv-printshiv.min.js");
}

const getImage = (imageName: string) => {
  return import(
    /* webpackInclude: /\.(png|svg|jpe?g|jpg|gif)$/i */
    /* webpackMode: "eager" */
    `./images/${imageName}.svg`
  ).then(module => {
    return module.default;
  });
};

const feedbackResponseMsg = {
  stc: "You have saved the call",
  swp: "You saved the call with parts",
  ns: "You have not saved the call"
};

class STCResponseMessage extends React.Component<any, any> {
  render() {
    const { message } = this.props;
    return <div className="message">{message.toUpperCase()}</div>;
  }
}
class FeedbackIcon extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
  }
  toggleIcon = e => {
    e.stopPropagation();
    e.preventDefault();
    e.nativeEvent.stopImmediatePropagation();
    this.props.selectedIcon(this.props.type);
  };
  render() {
    const { type, activeIcon } = this.props,
      src = `/images/${type}${
        activeIcon == type ? "_click_icon" : "_icon"
      }.svg`,
      selected = activeIcon == type ? "selected" : "";
    if (type === "swp") {
      return (
        <div className="disabled" title="This option is currently disabled">
          <img src={src} />
          <span>{type.toUpperCase()}</span>
        </div>
      );
    }
    return (
      <div onClick={this.toggleIcon} className={selected}>
        <img src={src} />
        <span>{type.toUpperCase()}</span>
      </div>
    );
  }
}
let timer = null;
class AppBar extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      activeIcon: null,
      respnseMessage: null,
      feedback: null,
      isIconClicked: false
    };
  }
  logout = () => {
    AuthService.forgetAuth().then(res => {
      this.props.signOut && this.props.signOut();
    });
  };
  toggleDrawer = () => {
    this.setState({
      feedback: null
    });
  };

  onIconClick = iconType => {
    const sessionId = SessionStore.get(Config.guidkey);
    this.setState({
      feedback: null,
      responseMessage: null
    });
    Service.feedbackSTC(sessionId, iconType)
      .then(data => {
        const { feedbackQuestion: feedback } = data;
        this.setState(
          {
            responseMessage: feedbackResponseMsg[iconType],
            isIconClicked: true
          },
          () => {
            if (timer)
              // @ts-ignore
              clearTimeout(timer);
            // @ts-ignore
            timer = setTimeout(() => {
              this.setState({
                feedback,
                responseMessage: null,
                activeIcon: null
              });
            }, 1500);
          }
        );
      })
      .catch(errRes => {
        this.setState({
          feedback: null,
          isIconClicked: true
        });
      });
    this.setState({
      activeIcon: iconType
    });
  };
  goToLaunchPage = () => {
    const { isIconClicked } = this.state;
    if (isIconClicked) {
      const path = "/launch";
      location.replace(path);
    } else {
      console.log("Not Allowed");
    }
  };
  componentDidMount() {
    document.onkeydown = e => {
      if (e.keyCode == 27) {
        if (this.state.feedback) {
          this.setState({
            feedback: null
          });
        }
      }
    };
    document.onclick = e => {
      this.setState({
        feedback: null
      });
    };
  }

  render() {
    const { activeIcon, responseMessage, feedback, isIconClicked } = this.state,
      {
        location: { pathname }
      } = this.props,
      src = isIconClicked
        ? "/images/exit_click_icon.svg"
        : "/images/exit_icon.svg",
      // checking if url is in format of /dashboard/:model/:serial then only showing feedback icons
      dashboardUrlRegex = /\/dashboard\/(GTW220|GTW330|GTW335|HTW200|HTW240)([A-Za-z]{3}[\d]?[A-Za-z]{2})?\/[ADFGHLMRSTVZ]{2}\d{6}G?/i,
      showFeedbackIcons = dashboardUrlRegex.test(pathname);

    return (
      <div className="app-header">
        <div className="branding">
          <img src={logo} />
          <h2 className="title">{Config.appname}</h2>
          {showFeedbackIcons
            ? [
                <div className="feedback-section" key="app_1">
                  <FeedbackIcon
                    type="stc"
                    activeIcon={activeIcon}
                    selectedIcon={this.onIconClick}
                  />
                  {/* <FeedbackIcon type="swp" activeIcon={activeIcon} selectedIcon={this.onIconClick} /> */}
                  <FeedbackIcon type="swp" />
                  <FeedbackIcon
                    type="ns"
                    activeIcon={activeIcon}
                    selectedIcon={this.onIconClick}
                  />
                </div>,
                <div
                  key="app_2"
                  className={`exit ${!isIconClicked ? "disabled" : ""}`}
                  onClick={this.goToLaunchPage}
                >
                  <img src={src} />
                </div>
              ]
            : null}
          {responseMessage ? (
            <STCResponseMessage message={responseMessage} />
          ) : null}
          <FeedBackDrawer
            toggleDrawer={this.toggleDrawer}
            feedBackData={feedback}
          ></FeedBackDrawer>
        </div>
      </div>
    );
  }
}

const AppBarWithRouter = withRouter(AppBar);
interface IAppProps {
  name: string;
}

interface IAppStates {
  logged: boolean;
}

class App extends React.Component<IAppProps, IAppStates> {
  static defaultProps = {
    name: ""
  };

  constructor(props: IAppProps) {
    super(props);
    this.state = {
      logged: false
    };
  }

  componentWillMount() {
    const token = AuthService.getAuthToken();
    this.setupApiClient();
    if (token) {
      this.setState({
        logged: true
      });
    }
  }

  componentDidMount() {
    // const token = AuthService.getAuthToken();
    // if(token) {
    //   Util.setCookie(Config.guidkey,  Util.guid(), 1);
    // }
    SessionStore.set(Config.guidkey, Util.guid());
    const sessionId = SessionStore.get(Config.guidkey);
    Service.feedbackSTC(sessionId, "")
      .then(data => {})
      .catch(errRes => {});
  }

  onAuth = res => {
    this.setState({
      logged: true
    });
  };

  signOut = () => {
    this.setState({
      logged: false
    });
  };

  getAuthToken() {
    // For Login Support
    // const user = AuthService.getAuth();
    // return user ? user.token : "";

    const token = SessionStore.get(Config.guidkey);
    return token || "";
  }

  setupApiClient() {
    Api.interceptor(context => {
      const {
        options,
        request: { headers },
        response
      } = context;
      headers.set("target", "va");
      if (!response) {
        // we are sending a request
        const useToken =
          typeof options.useToken === "undefined" ? true : options.useToken;
        if (useToken) {
          // const token = "Bearer " + (options.token || this.getAuthToken());
          const token = options.token || this.getAuthToken();
          if (token) headers.set("Authorization", token);
        }
      } else {
        return response.then(res => {
          // console.log(response);
          if (res.status === 401) {
            // this.refs.notifications.warn("We'll need to log you in first");
            console.log("Need To Login");
          } else if (res.status >= 400 && res.status <= 500) {
            // console.log("All ok");
            return Promise.reject(res);
          } else {
            return Promise.resolve(res);
          }
        });
      }
    });
  }
  render() {
    const token = AuthService.getAuthToken(),
      { logged } = this.state;
    return (
      <div className="app" id="app">
        <Router>
          <AppBarWithRouter logged={logged} signOut={this.signOut} />
          <div className="view">
            {/* {(logged
           ? (<ul className="nav-list">
                {routes.map((route, i) => {
                  return (route.path === "/login" || !route.path
                  ? null
                  : (<li key={route.path}>
                    <NavLink to={route.path}
                      activeClassName="selected"
                      exact={true}
                      isActive={(match, location) => {
                        if(!match)
                          return false;
                        else
                          return true;
                      }}>
                        {route.name || route.path}
                    </NavLink>
                  </li>));
                })}
              </ul>)
           : null)} */}
            <Switch>
              {routes.map((route, i) => {
                const self = this,
                  { component: Component, ...rest } = route;
                return route.auth ? (
                  <PrivateRoute key={i} {...route} />
                ) : (
                  <LoginRoute onAuth={this.onAuth} key={i} {...route} />
                );
              })}
            </Switch>
          </div>
        </Router>
      </div>
    );
  }
}

// window.onload = function() {
//   console.log("in BootStrap call")
//   BootStrap.init(`@configData@`);
// };
// @ts-ignore
const Bootstrap: any = (window.Bootstrap = {
  init(config: any) {
    const ROOT = document.querySelector(".root");
    console.log("Before Config : ", Config);
    Config.set(config);
    Api.setOption("apiUrl", Config.apiServerUrl + Config.apiBasePath);
    console.log("After Config : ", Config);
    ReactDOM.render(<App name="User" />, ROOT);
  }
});

export default Bootstrap;
