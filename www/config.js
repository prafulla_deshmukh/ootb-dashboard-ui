const _ = require("lodash");
module.exports = {
  appname: "Virtual Assistant",
  appnamespace: "vad",
  appversion: "1.0.0",
  apiServerUrl: "http://localhost:7001",
  apiBasePath: "/api",
  baseDir: "",
  guidkey: "__bps_guid__",
  agentCodeKey: "__bps_agentCode__",
  loggerType: "UI_INTERACTION_VADASHBOARD",
  botApi: {
    serverUrl: "https://geadevtestvm.bruviti.com:5050",
    path: "/agentbot/web/receive",
    useSocket: true,
  },
  set: function(config) {
    let self = this;
    try {
      const configObj = JSON.parse(config);
      Object.keys(configObj).forEach(function(prop) {
        if (prop !== "set") {
          self[prop] = _.isObject(configObj[prop]) ? Object.assign(self[prop], configObj[prop]) : configObj[prop];
        }
      });
    }
    catch (err) {

    }
  }
}
