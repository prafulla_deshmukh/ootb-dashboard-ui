/* global Promise setTimeout */
import ApiClient from "./lib/api-client";
import Config from './config';

const { instance: Api, asJson: resAsJson, asQueryParameters } = ApiClient;
export default {
  feedbackSTC(sessionId, code) {
    return Api.post("/feedback-service/stc/", {
      body: JSON.stringify({
        sessionId: sessionId,
        code: code
      })
    }).then(resAsJson)
      .then(res => {
        return res;
      })
  },
  feedbackAns(data) {
    return Api.post("/feedback-service/answerfeedback/", {
      body: JSON.stringify(data)
    }).then(resAsJson)
      .then(res => {
        return res;
      })
  }
};
