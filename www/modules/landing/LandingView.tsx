import * as React from "react";
import Config from "../../config";
import * as Util from "../../lib/util";

export class LandingView extends React.Component<any, any> {
  state = {
    errMsg: ""
  }
  componentWillMount() {
    const { history, location } = this.props;
    const queryParams = Util.getQueryParams(location.search);
    const { model, serial, problemCategory, description, agentCode } = queryParams;
    const query = Util.makeQueryParams({problemCategory, description});
    if (model && serial && problemCategory && agentCode) {
      Util.setCookie(Config.agentCodeKey, agentCode, 1);
      history.push(`/dashboard/${model}/${serial}?${query}`, { model, serial });
    } else {
      console.log("May be error in model serial problemCategory description guid");
      this.setState({
        errMsg: "Error"
      });
    }
  }

  render() {
    const { errMsg } = this.state;
    return (
      <div className="landing-view">
        <span>Welcome to the <span className="title">{Config.appname}</span> project</span>
      </div>
    );
  }
}
