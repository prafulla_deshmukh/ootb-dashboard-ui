/* global console setTimeout Promise */
import * as React from "react";
import {PageMessage} from "./PageMessage";

import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';

export class ErrorView extends React.Component<any, any> {
  static defaultProps = {
    error: {
      status: "404",
      message: "Page Not Found"
    }
  };
  routeTo = () => {
    const {history} = this.props;
    history.push("/");
  }
  render() {
    const {error = {}} = this.props;

    return (
      <div className="view error-view">
        <div className="content">
          <PageMessage iconConfig={{
            icon: faTimesCircle,
            size: "2x"
          }} message={error.status + " : " + error.message}>
          </PageMessage>
          <div className="actions">
            {/* <button className="inline primary" onClick={this.routeTo}>OK</button> */}
          </div>
        </div>
      </div>
    );
  }
};
