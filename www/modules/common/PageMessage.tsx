import * as React from "react";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faSpinner} from "@fortawesome/free-solid-svg-icons";
export class PageMessage extends React.Component<any, any> {
  static defaultProps = {
    iconConfig: {
      icon: faSpinner,
      size: "2x",
      pulse: true
    }
  }
  render() {
    const {message, iconConfig, children} = this.props,
      msg = message ? <p className="message">{message}</p> : null;
    return (
      <div className={`page-message`}>
        <span className="icon">
          <FontAwesomeIcon {...iconConfig} />
        </span>
        {msg}
        <div className="content">
          {children}
        </div>
      </div>
    );
  }
};
