import * as React from "react";
import * as ReactDOM from "react-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { Rating } from "./Rating";
import { Options } from "./Options";
import Service from "../../Service";
import { session as SessionStore } from "store2";
import Config from "../../config";

const CHECKBOX = "checkbox",
  RADIO = "radio",
  STAR = "star";

export type FeedBackType = "checkbox" | "radio" | "star";

export type IFeedBackDrawerProps = {
  feedBackData: any;
  toggleDrawer: Function;
  title?: String
};

export type IFeedBackDrawerStates = {
  ans: string[] | null;
};

export class FeedBackDrawer extends React.Component<IFeedBackDrawerProps, IFeedBackDrawerStates> {
  static defaultProps = {
    title: "Please spare a moment and choose an option(s) below."
  }
  constructor(props: IFeedBackDrawerProps) {
    super(props);

    this.state = {
      ans: null
    };
  }
  onAns = (ans: string[]) => {
    this.setState({ ans })
  }
  rendeOptions() {
    const { feedBackData: { type, options } } = this.props;
    return (type.toLowerCase() == STAR) ? <Rating onAns={this.onAns} totalStars={5} /> : <Options type={type.toLowerCase()} options={options} onAns={this.onAns}></Options>
  }
  renderQue() {
    const { feedBackData: { question } } = this.props;
    return (
      <div className="fd-content">
        <div className="que">{question}</div>
        <div className="que-opt">
          {this.rendeOptions()}
        </div>
      </div>
    )
  }
  componentDidMount() {
  }
  submitFeedBack = e => {
    const { feedBackData: { questionId } } = this.props;
    const { ans } = this.state;
    e.stopPropagation();
    e.preventDefault();
    e.nativeEvent.stopImmediatePropagation();
    if (!ans)
      return;
    const feebackData = {
      sessionId: SessionStore.get(Config.guidkey),
      questionId: questionId,
      answers: ans
    }
    Service.feedbackAns(feebackData).then(data => {
      // console.log("data: ", data);
      this.props.toggleDrawer();
    }).catch(errRes => {
      this.props.toggleDrawer();
    })
  }
  render() {
    const { title, feedBackData} = this.props,
      { ans } = this.state;
    return (
      <div className={`feedback-drawer ${feedBackData ? 'show' : ''}`} onClick={(e) => {
        e.stopPropagation();
        e.preventDefault();
        e.nativeEvent.stopImmediatePropagation();
      }}>
        <div className="fd-header">
          <div onClick={() => {
            this.props.toggleDrawer();
          }} className="close-icon">
            <FontAwesomeIcon icon={faTimes} />
          </div>
          {
            (feedBackData && feedBackData.type.toLowerCase() == STAR)
              ? ""
              : <span className="fd-title">{title}</span>
          }
        </div>
        {feedBackData ? this.renderQue() : null}
        <div className="fd-footer">
          <button type="submit" className="submit-btn button primary inline" disabled={ans ? false : true} onClick={this.submitFeedBack}>Submit</button>
        </div>
      </div>
    );
  }
}