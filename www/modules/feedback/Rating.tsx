import * as React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-regular-svg-icons';

export type IRatingProps = {
  rating?: number | null;
  totalStars?: number;
  onAns: (ans: string[]) => void;
};

export type IRatingStates = {
  // disabled: boolean;
  rating: number | null;
  temp_rating: number | null;
};

export class Rating extends React.Component<IRatingProps, IRatingStates> {
  static defaultProps = {
    totalStars: 5,
  };

  constructor(props: IRatingProps) {
    super(props);

    this.state = {
      // disabled: false,
      rating: this.props.rating || null,
      temp_rating: null,
    };
  }

  componentDidMount(): void {
    // console.log('Rating component mounted');
  }

  rate = (rating: number) => {
    const rate = rating;
    this.setState({
      // disabled: true,
      rating: rate,
      temp_rating: rate
    }, () => {
      const { onAns } = this.props;
      onAns && onAns([`${rate}`]);
    });
  }

  star_over = (rate: number) => {
    const { rating } = this.state;

    this.setState({
      rating: rate,
      temp_rating: rating
    });
  }

  star_out = () => {
    const { temp_rating } = this.state;
    this.setState({ rating: temp_rating })
  }

  /** renders the component */
  render() {
    const { state: { rating }, props: { totalStars } } = this;
    let stars: any[] = [];

    //@ts-ignore
    for (let i = 1; i < totalStars + 1; i++) {
      stars.push(
        <div key={i} className={`star
              ${(rating != null && rating >= i) ? "selected" : ""}`}
          onClick={(e) => {
            // if (!disabled)
            e.stopPropagation();
            e.preventDefault();
            e.nativeEvent.stopImmediatePropagation();
            this.rate(i);
          }}
          onMouseOver={() => {
            // if (!disabled)
            this.star_over(i);
          }}
          onMouseOut={() => {
            // if (!disabled)
            this.star_out();
          }}
        >
          <FontAwesomeIcon icon={faStar} />
        </div>
      );
    }

    return (
      <div className="star-rating">
        {stars}
      </div>
    );
  }
}
