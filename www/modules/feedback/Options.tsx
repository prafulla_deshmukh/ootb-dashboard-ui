import * as React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircle, faDotCircle, faCheckCircle } from '@fortawesome/free-regular-svg-icons';
import * as _ from "lodash";

const optIcon = {
  "radio": faCircle,
  "radio-selected": faDotCircle,
  "checkbox": faCircle,
  "checkbox-selected": faCheckCircle
}
export type OptionsType = 'checkbox' | 'radio';

export type IOptionsProps = {
  options: string[];
  onAns: (ans: string[]) => void;
  type: OptionsType
};

export type IOptionsStates = {
  ans: string[] | [];
};

export class Options extends React.Component<IOptionsProps, IOptionsStates> {
  static defaultProps = {
    options: null,
  };

  constructor(props: IOptionsProps) {
    super(props);

    this.state = {
      ans: [],
    };
  }

  componentDidMount(): void {
    // console.log('Rating component mounted');
  }
  onSelect(opt: string) {
    const { type, onAns } = this.props;
    let ans: string[] = [];

    if (type.toLowerCase() == "radio") {
      ans = [opt]
      this.setState({
        ans
      });
    } else {
      ans = _.includes(this.state.ans, opt) ? this.state.ans.filter(e => e !== opt) : [...this.state.ans, opt];
      this.setState({
        ans
      });
    }
    onAns && onAns(ans);
  }

  renderOptions() {
    const { state: { ans }, props: { options, type } } = this;
    return <ul className={`opt-container ${type.toLowerCase()}`}> {
      options && options.map(opt => {
        const active = _.includes(ans, opt)
        const optIc = `${type.toLowerCase()}${active ? "-selected" : ""}`
        return (
          <li className={`option${active ? " selected" : ""}`} key={opt} onClick={(e) => {
            e.stopPropagation();
            e.preventDefault();
            e.nativeEvent.stopImmediatePropagation();
            this.onSelect(opt);
          }}>
            <span className="opt-icon">
              <FontAwesomeIcon icon={optIcon[optIc]} />
            </span>
            <span className="opt-label">{opt}</span>
          </li>
        )
      })
    }
    </ul>

  }
  render() {
    return (
      <div className="options">
        {this.renderOptions()}
      </div>
    );
  }
}
