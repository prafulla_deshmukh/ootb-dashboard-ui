import {PrivateRoute} from "./PrivateRoute";
import routes from "./routes";
import Service from "./Service";


export {
  routes,
  Service,
  PrivateRoute
};