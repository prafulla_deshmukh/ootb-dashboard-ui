/* global history */
import * as React from "react";
import Service from "./Service";
import { Redirect } from "react-router-dom";

type IAuthComponentStates = {
  busy: boolean;
  auth: object;
  error: object;
  message: string;
  username: string;
  password: string;
}

export class AuthComponent extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      busy: false,
      auth: null,
      error: null,
      message: "Please sign in using your username/email and password",
      username: "",
      password: ""
    }
  }

  handleChange = (event, field) => {
    this.setState({[field]: (event.target.value).trim()});
  }

  authenticate = () => {
    const {username, password} = this.state;
    if(username && password) {
      this.setBusy(true, "Checking, please wait...");
      let {history} = this.props;
      Service.auth(username, password).then(res => {
        if(res.token) {
          this.setAuthSuccess(res);
          this.props.onAuth && this.props.onAuth(res);
          history.push("/");
        }else {
          this.setAuthError(res);
        }
      }).catch(err => {
        console.log(err);
        this.setBusy(false);
        const msg = err.message || err.statusText;
        this.setAuthError({
          url: err.url,
          status: err.status,
          message: err.status === 401 ? "Wrong username or password?" : msg,
          statusText: err.statusText
        });
      });
    } else {
      if(!username && !password)
        this.setBusy(false, "Please enter your username/email and password");
      else if(!username)
        this.setBusy(false, "Please enter your username/email");
      else
        this.setBusy(false, "Please enter your password");
    }
  }

  handleEnter = (e) => {
    if(e.charCode === 13) {
      this.authenticate();
    }
  }

  setBusy = (busy = true, message = busy ? "Please wait..." : "") => {
    this.setState({
      busy: busy,
      message: message,
      error: null
    });
  }

  setAuthSuccess = (data) => {
    this.setState({
      busy: false,
      auth: data,
      error: null,
      message: "Login successfully"
    });
  }

  setAuthError = (reason) => {
    this.setState({
      busy: false,
      auth: null,
      error: reason,
      message: "Could not sign in: " + (reason.message || "Unknown error")
    });
  }

  render() {
    const {busy, error, message, username, password} = this.state;
    return (
      <div className="login-view">
      <div className="login-component">
        <div className="login-dialog-box">
          <div className="header">
            <span className="title">SIGN IN</span>
          </div>
          <div className="body">
            <p className={`message ${error ? "error" : ""}`}>
              <i className="icon-alert-circle"></i> {message}
            </p>
            <div className="form">
              <div className={`field-container`} >
                <input type="text" placeholder="Username" value={username} onChange={(evt) => {
                  this.handleChange(evt, "username");
                }} onKeyPress={this.handleEnter} />
              </div>
              <div className={`field-container`} >
                <input type="password" placeholder="Password" value={password} onChange={(evt) => {
                  this.handleChange(evt, "password");
                }} onKeyPress={this.handleEnter} />
              </div>
            </div>
            <div className="actions">
              <button className="button primary inline" onClick={this.authenticate} disabled={busy}>Sign Up</button>
            </div>
          </div>
        </div>
      </div>
      </div>
    );
  }
};
