import {AuthComponent} from "./AuthComponent";

export default [
  {
    path: "/login",
    component: AuthComponent,
    exact: true,
    name: "Login",
    auth: false
  }
];