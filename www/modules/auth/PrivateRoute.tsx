import * as React from "react";
import {Route, Redirect} from "react-router-dom";

import Service from "./Service";

export class PrivateRoute extends React.Component<any, any> {
  render() {
    const {component:Component, ...rest} = this.props,
      token = Service.getAuthToken();

    return (
      <Route
        {...rest}
        render={props => (
          token
            ? <Component {...props} />
            : <Redirect
                to={{
                  pathname: "/login",
                  state: { from: props.location }
                }}
              />)
        }
      />
    );
  }
};
