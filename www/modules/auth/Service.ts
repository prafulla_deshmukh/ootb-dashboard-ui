/* global Promise FormData console */
import ApiClient from "../../lib/api-client";
import Config from "../../config";
import Store from "store2";

const Storage = Store.namespace(Config.appnamespace),
    {instance: Api, asJson: resAsJson} = ApiClient,
    getApiUrl = () => {
      const {apiServerUrl, apiBasePath} = Config;
      return apiServerUrl ? `${apiServerUrl}${apiBasePath}` : apiBasePath;
    },
    AnonApi = ApiClient.create({
      apiUrl: getApiUrl()
    }),
    VerificationApi = ApiClient.create({
      apiUrl: Config.apiServerUrl,
      useToken: false,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    }),
    ensureHttp200 = res => {
      if(res.status >= 200 && res.status < 400) {
        return res;
      }else {
        return Promise.reject(res);
      }
    },
    resAsText = res => {
      if(res.status >= 200 && res.status < 400) {
        return res.text();
      }else {
        return Promise.reject(res);
      }
    };

let authInfo = null;

const AuthService = {
  auth(username, password) {
    return AnonApi.post("/auth", {
      useToken: false,
      // body: `username=${username}&password=${password}`
      body: JSON.stringify({
        username: username,
        password: password
      })
    }).then(resAsJson).then(user => {
      // console.log("Auth Serice", user);
      this.setAuth(user);
      return user;
    });
  },

  setAuth(user) {
    authInfo = user;
    this.storeAuth(user);
  },

  getAuth() {
    return authInfo || Storage.get("Auth");
  },

  storeAuth(user) {
    Storage.set("Auth", user);
  },

  getAuthToken() {
    const auth = this.getAuth();
    return auth ? auth.token : "";
  },

  forgetAuth() {
    return Api.post("/logout").then(response => {
      this.clearAuth();
      return response;
    }).catch(err => {
      console.log(err);
      this.clearAuth();
    });
  },

  clearAuth() {
    authInfo = null;
    Storage.remove("Auth");
  }
};

export default AuthService;
