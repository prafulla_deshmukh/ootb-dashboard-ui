import * as React from "react";
import {Route, Redirect} from "react-router-dom";

import Service from "./Service";

export class LoginRoute extends React.Component<any, any> {
  render() {
    const {component:Component, onAuth, ...rest} = this.props,
      token = Service.getAuthToken();

    return (
      <Route
        {...rest}
        render={props => (
          token
            ? <Redirect
                to={{
                  pathname: "/",
                  state: { from: props.location }
                }}
              />
            : <Component onAuth={onAuth} {...props} />)
        }
      />
    );
  }
};
