// Widget registration happens here
import "./widgets";
import Config from "../../config"

const getDashboardConfig = (deviceInfo) => {
  return {
    "name": "My Dashboard",

    "style": {
      "cssClass": "appliance-dashboard"
    },

    "datalayer": {
      "name": "Appliance Data"
      // "channels": "/appliance" 
      // [ 
      //   {
      //     "name": "/appliance",
      //     "factory": function(config) {
      //       return {
      //         open: function(context) {
      //           context.emit("data", deviceInfo);
      //         },
      //         close: function() {}
      //       };
      //     }
      //     "factory": function(config) {
      //       return {
      //         open: function(context) {
      //           context.emit("data", deviceInfo);
      //         },
      //         close: function() {}
      //       };
      //     }
          
      //   }
      // ]
    },
    "widgets": [
      {
        id: "MediaWidget",
        type: "MediaWidget",
        // title: "Media",
        cols: 5,
        name: "Media content...",
        channel: ["/issue-history-by-serial", "/issue-history-by-model", "/faq", "/manual", "/top-issues", "/video"]
      },
      {
        id: "appliance-info",
        type: "ApplianceInfoWidget",
        title: "Appliance Info",
        cols: 7,
        name: "Appliance Info content...",
        channel: "/appliance"
      },
      // {
      // id: "DummyWidget",
      // type: "DummyWidget",
      // cols: 5,
      // panelClass: "widget-hidden"
      // },
      {
        id: "Chatbot",
        type: "ChatbotWidget",
        title: "Chatbot",
        cols: 7,
        name: "Chatbot content...",
        panelClass: "chatbot",
        channel: "/chatbot"
      }
    ]
  };
};


export default (deviceInfo) => {
    return getDashboardConfig(deviceInfo);
};
