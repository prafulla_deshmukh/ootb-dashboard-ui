import {DashboardView} from "./View";

export default [
  {
    path: "/dashboard/:model/:serial",
    component: DashboardView,
    exact: true,
    name: "Dashboard",
    auth: false
  }
];
