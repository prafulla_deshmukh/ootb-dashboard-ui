import * as React from "react";
import { WidgetPanel, AsSubscribedComponent } from "bru-web-sdk/packages/bru-ui-dashboard";
import { ApplianceInfo } from './ApplianceInfoComp';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';

class ApplianceInfoWidget extends React.Component<any, any> {

  constructor(props: any) {
    super(props);
    this.state = {
      data: {
      },
      loading: true
    };
  }
  componentDidUpdate() {
    this.props.layout.layout();
  }
  render() {
    const { Header, Content } = WidgetPanel,
      { data, loading } = this.state;
    return (
      <WidgetPanel {...this.props}>
        <Header>{this.props.title}</Header>
        <Content>
          <div className="appliance-info-content">
            {
              loading ? <div className="loading-indicator">
                <FontAwesomeIcon icon={faSpinner} spin={true} pulse={true} size="2x" />
                <p className="loading-txt">Loading...</p>
              </div> :
                <ApplianceInfo data={data} context={this.props.context}/>
            }
          </div>
        </Content>
      </WidgetPanel>
    );
  }

  onData(data, channel) {
    this.setState({
      channel: channel,
      data: data,
      loading: false
    });
  }
};

const Component = AsSubscribedComponent(ApplianceInfoWidget);

const Info = {
  id: "ApplianceInfoWidget",
  type: Component, // The widget we defined above
  name: "Appliance Info", // The name of the widget
  description: "A simple widget"
};

export default {
  Info,
  ApplianceInfoWidget: Component
};
