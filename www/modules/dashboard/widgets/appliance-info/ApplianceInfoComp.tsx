import * as React from "react";
import * as ReactDOM from "react-dom";
import ApplianceImage from "./images/no_image.svg";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes, faArrowAltCircleRight, faArrowAltCircleLeft, faSpinner } from '@fortawesome/free-solid-svg-icons';
import { wheelzoom } from "../../../../lib/wheelzoom";
import Service from "../../Service";
interface IAppAttributeProps {
  label: string;
  value: string;
};

interface IAppAttributeStates { };

const getImage = (imageName: string, ext: string = ".jpeg") => {
  // return Promise.all([
  //   import(
  //     /* webpackInclude: /\.(png|svg|jpe?g|jpg|gif)$/i */
  //     /* webpackMode: "eager" */
  //     `./images/${imageName}/${imageName}${ext}`
  //   ),
  //   import(
  //     /* webpackInclude: /\.(png|svg|jpe?g|jpg|gif)$/i */
  //     /* webpackMode: "eager" */
  //     `./images/${imageName}/${imageName}_panel${ext}`
  //   )
  // ]).then(([applianceImg, panelImg]) => {
  //   return {
  //     applianceImg: applianceImg.default,
  //     panelImg: panelImg.default
  //   };
  // });
  return import(
    /* webpackInclude: /\.(png|svg|jpe?g|jpg|gif)$/i */
    /* webpackMode: "eager" */
    `./images/${imageName}${ext}`
  ).then(module => {
    return module.default;
  })
}

class ImageDialog extends React.Component<any, any> {
  imgRef: any = null;
  static defaultProps = {
    src: "",
    selected: 0,
    thumbnails: "",
    context:{}
  }
  state = {
    selected: this.props.selected,
    imgSrc: "",
    busy: true,
    imgErr: false,
    thumbnails: []
  }
  constructor(props) {
    super(props);
  }
  handleClose = _ => {
    if (this.props.closed)
      this.props.closed();
  }
  imgPromise = (nextImg, imgArr) => {
    return getImage(nextImg).then(img => {
      return [...imgArr, img];
    }).catch(errMsg => {
      return [...imgArr, ApplianceImage];
    })
  }
  getThumbnailImages = () => {
    const { thumbnails, src } = this.props;
    if (thumbnails) {
      let resultIMG = thumbnails.reduce((accumulatorPromise, currentImg) => {
        return accumulatorPromise.then((imgArr) => {
          return this.imgPromise(currentImg, imgArr);
        });
      }, Promise.resolve([]));

      resultIMG.then(thumbnails => {
        this.setState({
          thumbnails
        });

      }, this.scrollToview());
    }
  }
  renderThumbnails = () => {
    const { thumbnails, selected } = this.state;
    let thumbImgArr: any[] = [];

    if (thumbnails) {
      thumbnails.forEach((img, ind) => {
        const style = {
          backgroundImage: `url(${img})`
        };
        thumbImgArr = [...thumbImgArr, <a id={`thumb-scroll-${ind}`} onClick={() => this.selectImg(ind)} key={`thumb-${ind}`} className={`thumbnail${selected === ind ? ' active' : ''}`}>
          <img src={img}></img>
        </a>]
      });
    }
    return thumbImgArr || null;
  }
  scrollToview() {
    const { state: { selected } } = this;
    const elmnt = document.getElementById(`thumb-scroll-${selected}`);
    elmnt && elmnt.scrollIntoView();
  }
  componentDidMount() {
    wheelzoom(this.imgRef);
    const { props: { src }, state: { selected } } = this;
    this.getThumbnailImages();
    this.getImages(src[selected]);
    setTimeout(() => {this.scrollToview()}, 300)
  }
  selectImg = (ind) => {
    const { props: { src } } = this;
    this.setState({
      selected: ind,
      busy: true
    }, () => {
      this.getImages(src[this.state.selected])
    });
  }
  prev = () => {
    const { props: { src }, state: { selected } } = this;
    this.setState({
      selected: selected > 0
        ? selected - 1
        : src.length - 1,
      busy: true
    }, () => {
      this.getImages(src[this.state.selected])
    });
  }
  next = () => {
    const { selected } = this.state,
      { src } = this.props;
    this.setState({
      selected: (selected + 1) % src.length,
      busy: true
    }, () => {
      this.getImages(src[this.state.selected])
    });
  }
  getImages(imgName) {
    this.scrollToview();
    getImage(imgName).then(img => {
      this.setState({
        imgSrc: img,
        imgErr: false,
        busy: false
      }, () => {
        this.sendLog();
      })
    }).catch(errMsg => {
      this.setState({
        imgSrc: ApplianceImage,
        imgErr: true,
        busy: false
      }, () => {
        this.sendLog();
      })
      return errMsg;
    })
  }
  sendLog() {
    const { context: { problemCategory, description, applianceInfo: {model, serial} } } = this.props;
    if (this.state.imgErr)
      return;
    const logData = {
      event: "click",
      targetType: "Backsplash",
      message: "Appliance Info image",
      target: this.state.imgSrc,
      problemCategory,
      description,
      model,
      serial
    }
    Service.log(logData);
  }
  render() {
    const { src } = this.props,
      { selected, imgSrc, busy, imgErr } = this.state,
      isSrcArray = src.constructor == Array;

    return (
      <div className="bru-img-dialog">
        <div className="img-container">
          <div className="icon-container">
            {isSrcArray && src.length > 0 ? <span className="img-counter">{`${selected + 1}/${src.length}`}</span> : null}
            <div className="close-icon" onClick={this.handleClose}>
              <FontAwesomeIcon icon={faTimes} size="1x" />
            </div>
          </div>
          {busy ? <div className="loading-indicator">
            <FontAwesomeIcon icon={faSpinner} spin={true} pulse={true} size="4x" />
            <p className="loading-txt">Loading...</p>
          </div> : null
          }
          <span className="img-mul">
            {
              //@ts-ignore
              isSrcArray && src.length > 0 ? <FontAwesomeIcon className="icon-left" icon={faArrowAltCircleLeft} size="1x" onClick={this.prev} /> : null
            }
            <span className="img-holder">
              <img className={`zoom ${isSrcArray && src.length > 0 ? 'img-w2' : 'img-w1'} ${imgErr ? 'img-err' : ''}`} ref={x => this.imgRef = x} src={imgSrc} />
              {/* {isSrcArray && src.length > 0 ? <span>{`${selected + 1}/${src.length}`}</span> : null} */}
              <div className="thumbnail-holder">
                {this.renderThumbnails()}
              </div>
            </span>
            {
              //@ts-ignore
              isSrcArray && src.length > 0 ? <FontAwesomeIcon className="icon-right" icon={faArrowAltCircleRight} size="1x" onClick={this.next} />
                : null
            }
          </span>
        </div>
      </div>
    );
  }
}

class AppAttribute extends React.Component<IAppAttributeProps, IAppAttributeStates> {
  static defaultProps = {
    label: "",
    value: ""
  }
  constructor(props) {
    super(props);
  }
  render() {
    const { label, value } = this.props;
    return (
      <div>
        <label>{label}</label>
        <span>{value || "N/A"}</span>
      </div>
    );
  }
}

type Data = {
  type: string,
  model: string,
  serial: string,
  imgUrl: string,
  features: string[]
};

export type IApplianceInfoProps = {
  data: Data,
  context?: any
};

export type IApplianceInfoStates = {
  imgSrc: string,
  modelImages: string | string[],
  thumbnailImages: string | string[],
  showLightBox: boolean
  imgErr: boolean
};
export class ApplianceInfo extends React.Component<IApplianceInfoProps, IApplianceInfoStates> {

  state = {
    imgSrc: ApplianceImage,
    modelImages: ApplianceImage,
    thumbnailImages: ApplianceImage,
    showLightBox: false,
    imgErr: false
  }
  getApplainceImage(modelFamily) {
    getImage(`${modelFamily}/${modelFamily}`).then(applianceImg => {
      this.setState({
        imgSrc: applianceImg,
        imgErr: false
      })
    }).catch(errMsg => {
      this.setState({
        imgSrc: ApplianceImage,
        imgErr: true
      })
      return errMsg;
    })
  }
  componentDidMount() {
    const { data: { model = "" } } = this.props,
      modelFamily = model && model.toUpperCase().slice(0, 6);
    if (model) {
      this.getApplainceImage(modelFamily);
      this.setState(Service.getModelImgs(modelFamily));
      // {
      //   modelImages: Service.getModelImgs(modelFamily).modelImages,
      //   thumbnailImages: Service.getModelImgs(modelFamily).thumbnailImages,
      // })
    }
  }
  componentWillReceiveProps(nextProps) {
    const { data: { model = "" } } = nextProps,
      modelFamily = model && model.toUpperCase().slice(0, 6);
    if (model) {
      this.getApplainceImage(modelFamily);
    }
  }
  onPanelImageClick = () => {
    const { context: { problemCategory, description, applianceInfo: {model, serial} } } = this.props;
    if (this.state.imgErr)
      return;

    this.setState({
      showLightBox: !this.state.showLightBox
    })
    const logData = {
      event: "click",
      targetType: "Backsplash",
      message: "Appliance Info image",
      target: this.state.imgSrc,
      problemCategory,
      description,
      model,
      serial
    }
    Service.log(logData);
  }
  handlePanelClose = () => {
    this.setState({
      showLightBox: !this.state.showLightBox
    })
  }
  render() {
    const { data: { type, model = "", serial, features, imgUrl } } = this.props,
      { imgSrc, modelImages, showLightBox, imgErr, thumbnailImages } = this.state;
    return (
      <div className="appliance-info">
        <div className={`appInfo ${!features ? "no-features" : ""}`}>
          <div className={`appInfo-img ${!features ? "no-features" : ""}${imgErr ? " img-err" : ""}`}>
            <img src={imgSrc} onClick={this.onPanelImageClick} />
            {/* <img src={`/modules/dashboard/widgets/appliance-info/images/GTW220.jpeg`} /> */}
          </div>
          <div className="app-info">
            <AppAttribute label="Type" value={type} />
            <AppAttribute label="Model #" value={model} />
            <AppAttribute label="Serial #" value={serial} />
          </div>
        </div>
        {/* <div className="panel-info">
          <img src={panelImg} onClick={this.onPanelImageClick}/>
        </div> */}
        {features
          ? <div className="features">
            <p>2015 Program Top Load Washers</p>
            <div className="features-val-section">
              {features.map((feature, i) => {
                return (
                  <div key={i} className="value" title={feature}>{feature}</div>
                )
              })}
            </div>
          </div>
          : null
        }
        {
          showLightBox ? <ImageDialog context={this.props.context} src={modelImages} thumbnails={thumbnailImages} closed={this.handlePanelClose} /> : null
        }
      </div>
    );
  }
}