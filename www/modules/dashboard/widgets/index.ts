import {WidgetRegistry} from "bru-web-sdk/packages/bru-ui-dashboard";

import ApplianceInfoWidget from "./appliance-info/ApplianceInfoWidget";
import ChatbotWidget from "./chat-bot/ChatbotWidgets";
import MediaWidget from "./media/MediaWidget";
import DummyWidget from "./dummy/DummyWidget";

WidgetRegistry.add(ApplianceInfoWidget.Info.id, ApplianceInfoWidget.Info);
WidgetRegistry.add(ChatbotWidget.Info.id, ChatbotWidget.Info);
WidgetRegistry.add(MediaWidget.Info.id, MediaWidget.Info);
WidgetRegistry.add(DummyWidget.Info.id, DummyWidget.Info);
