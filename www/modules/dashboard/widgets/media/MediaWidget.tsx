import * as React from "react";
import { WidgetPanel, AsSubscribedComponent } from "bru-web-sdk/packages/bru-ui-dashboard";
import { FAQComponent } from "./FaqComponent";
import { ResourceComponent } from "./ResourceComponent";
import { Top5Issues } from "./Top5IssuesComp";
import { IssueHistoryComponent } from "../issue-history/IssueHistoryComponent";
import { Drawer } from "../drawer/Drawer";
import Service from "../../Service";

class MediaWidget extends React.Component<any, any> {

  constructor(props: any) {
    super(props);
    this.state = {
      issueHistoryModel: [],
      issueHistorySerial: [],
      faqData: {},
      manualData: {},
      videoData: {},
      top5Issues: [],
      sidePanel: false,
      selectedMessage: {}
    };
  }
  componentDidUpdate() {
    this.changeLayout();
  }

  changeLayout = () => {
    this.props.layout.layout();
  }

  onActions(action, data) {
    const { onAction } = this.props;
    onAction && onAction(action, data);
  }

  // nextPage = (pageNumber: number) => {
  //   this.onActions("updateRecords/serial", {
  //     pageNumber
  //   });
  // }

  // previousPage = (pageNumber: number) => {
  //   this.onActions("updateRecords/serial", {
  //     pageNumber
  //   });
  // }

  changeTab = (tabIndex: number, label: string) => {
    const { context: { contentTypeMap, openNative, problemCategory, description, applianceInfo: {model, serial} } } = this.props;
    if (tabIndex === 1) {
      this.onActions("updateRecords/searchTerm", {});
    } 
    const logData = {
      event: "click",
      targetType: "ServiceHistory",
      message: tabIndex === 0 ? "STC ISSUES" : "TOP 5 ISSUES",
      problemCategory,
      description,
      model,
      serial,
      target: tabIndex === 0 ? "stcIssues" : "top5Issues"
    }
    Service.log(logData);
    /* else {
      this.onActions("updateRecords/serial", {
        // pageNumber: 0
      });
    } */
  }
  toggleDrawer = () => {
    this.setState({
      sidePanel: !this.state.sidePanel
    }, () => {
      this.onActions("updateRecords/drawerContent", {});
    });
  }
  popOut = () => {
    const { resource, text } = this.state.selectedMessage;
    if (resource) {
      window.open(resource, '_blank');
    } else if (text) {
      const w = window.open() as Window;
      w.document.write(text);
    }
  }
  onMediaMsgClick = (data: any) => {
    const { context: { contentTypeMap, openNative, problemCategory, description, applianceInfo: {model, serial} } } = this.props,
      { title, category, resource, text } = data;

    this.setState({
      selectedMessage: data
    })

    const logData = {
      event: "click",
      targetType: category,
      target: resource,
      message: title,
      problemCategory,
      description,
      model,
      serial
    }
    Service.log(logData);

    let native = false;
    if (openNative) {
      native = openNative.some(ele => ele.category === category)
    }
    if (native) {
      window.open(resource, '_blank');
      this.onActions("updateRecords/drawerContent", {});
      return;
    }
    this.toggleDrawer();
  }
  componentWillReceiveProps(nextProps) {
    const { context: { selectedMsg, contentTypeMap } } = nextProps;
    if (selectedMsg) {
      const { contentType, search, text, url, title } = selectedMsg,
        msgData = {
          title,
          category: contentTypeMap[contentType],
          resource: url,
          text
        }
      this.onMediaMsgClick(msgData);
    }
  }
  render() {
    const { issueHistoryModel, issueHistorySerial,faqData, manualData, videoData, top5Issues, channel,
      sidePanel, selectedMessage } = this.state,
      { context: { applianceInfo, searchTerm } } = this.props;
    return (
      <div className="widget-wrap">
        <div className="wrap">
          <div className="widget-panel">
            <div className="header">
              Issue History
            </div>
            <div className="content">
              <IssueHistoryComponent layout={this.changeLayout}
                dataModel={issueHistoryModel} dataSerial={issueHistorySerial} channel={channel}
                // nextPage={this.nextPage}
                // previousPage={this.previousPage}
                changeTab={this.changeTab}
                applianceInfo={applianceInfo} />
            </div>
          </div>
        </div>
        <div className="wrapper">
          <div className="left">
            <div className={`wrap ${searchTerm ? "top5-hidden" : ""}`}>
              <div className="widget-panel">
                <div className="header">FAQS</div>
                <div className="content">
                  <FAQComponent data={faqData} onMediaMsgClick={this.onMediaMsgClick} />
                </div>
              </div>
            </div>
            {!searchTerm
              ? <div className="wrap">
                <div className="widget-panel">
                  <div className="header">Top 5 Issues</div>
                  <div className="content">
                    <Top5Issues data={top5Issues} />
                  </div>
                </div>
              </div>
              :
              null
            }
          </div>
          <div className="right">
            <div className="wrap">
              <div className="widget-panel">
                <div className="header">Manuals & Service Bulletins</div>
                <div className="content">
                  <ResourceComponent data={manualData} onMediaMsgClick={this.onMediaMsgClick} />
                </div>
              </div>
            </div>
            <div className="wrap">
              <div className="widget-panel">
                <div className="header">Videos</div>
                <div className="content">
                  <ResourceComponent data={videoData} onMediaMsgClick={this.onMediaMsgClick} />
                </div>
              </div>
            </div>
          </div>
        </div>
        {sidePanel
          ? <Drawer sidePanel={sidePanel} toggleDrawer={this.toggleDrawer}
            selectedMessage={selectedMessage} popOut={this.popOut} searchTerm={searchTerm} />
          : null
        }
      </div>
    );
  }

  onData(data, channel) {
    switch (channel) {
      case '/issue-history-by-serial':
        this.setState({
          channel: channel,
          issueHistorySerial: data
        });
        break;
      case '/issue-history-by-model':
        this.setState({
          channel: channel,
          issueHistoryModel: data
        });
        break;
      case '/faq':
        this.setState({
          channel: channel,
          faqData: data
        });
        break;
      case '/manual':
        this.setState({
          channel: channel,
          manualData: data
        });
        break;
      case '/top-issues':
        this.setState({
          channel: channel,
          top5Issues: data.issues
        });
        break;
      case '/video':
        this.setState({
          channel: channel,
          videoData: data
        });
        break;
      default:
        console.log("No channel found");

    }
  }
};

const Component = AsSubscribedComponent(MediaWidget);

const Info = {
  id: "MediaWidget",
  type: Component, // The widget we defined above
  name: "Media", // The name of the widget
  description: "A simple widget"
};

export default {
  Info,
  MediaWidget: Component
};
