import * as React from "react";
import * as ReactDOM from "react-dom";

type Data = {
  section: string;
  error_percentage: number;
  category: string;
};

export type Itop5IssuesProps = {
  data: Data[]
};

export type ITop5IssuesStates = {};
export class Top5Issues extends React.Component<Itop5IssuesProps, ITop5IssuesStates> {
  render() {
    const { data: issues} = this.props;
    return (
      <div className="sub-content">
        {
          // issues && issues.length > 0 ?
          <ul className="top-issues">
            {issues.map(issue => {
              const recurrance = issue.error_percentage ? issue.error_percentage : "0";
              return (
                <li key={issue.section}>
                  <span title={issue.section} className="category">
                    {issue.section}
                  </span>
                  <div className="issue-bar" style={{ width: recurrance + "px" }}> </div>
                  <span className="width-label">{recurrance + "%"}</span>
                </li>
              )
            })
            }
          </ul>
          // : <div className="no-resources">No top5 issues for this appliance</div>
        }
      </div>
    );
  }
}