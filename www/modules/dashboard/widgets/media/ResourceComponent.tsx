import * as React from 'react';
import videoPlayIcon from "../images/video_play_reviewer.png";
import manualPdfIcon from "../images/pdf_icon.svg";
import Service from "../../Service";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faThumbsUp as faThumbsUpFilled, faThumbsDown as faThumbsDownFilled } from '@fortawesome/free-solid-svg-icons';
import { faThumbsUp, faThumbsDown } from '@fortawesome/free-regular-svg-icons';

export type IResourceData = {
  description: string,
  title: string,
  resource: string,
  imgUrl: string,
}
export type IData = {
  resources: IResourceData[],
  category: string,
}

export type IResourceProps = {
  data: IData,
  onMediaMsgClick: Function
}

const categoryIcon = {
  Video: videoPlayIcon,
  Manual: manualPdfIcon
}
export type IResourceStateData = {
  description: string,
  title: string,
  resource: string,
  imgUrl: string,
  feedback: string
}
export type IResourceState = {
  resources: IResourceStateData[]
}
export class ResourceComponent extends React.Component<IResourceProps, IResourceState> {
  constructor(props) {
    super(props);
    this.state = {
      resources: []
    };
  }
  componentWillReceiveProps(nextProps) {
    const { resources } = nextProps.data;
    if (resources && resources.length > 0) {
      let resourcesData = [] as any;
      resourcesData = resources.map(data => {
        return {
          description: data.description,
          title: data.title,
          resource: data.resource,
          imgUrl: data.imgUrl,
          feedback: 'ignore'
        };
      });
      this.setState({
        resources: resourcesData
      })
    }
  }
  static defaultProps = {
    data: [{
      resource: "#",
      title: "Document Title",
      description: "Document Subtitle",
      imgUrl: "https://products.geappliances.com/MarketingObjectRetrieval/Dispatcher?RequestType=Image&Name=162299.jpg&Variant=ViewLarger"
    }]
  };
  onResourceClick(data) {
    this.props.onMediaMsgClick && this.props.onMediaMsgClick(Object.assign(data, { category: this.props.data && this.props.data.category }));
  }
  updateFeedback = (data, index, feedback) => {
    const { resources } = this.state;
    data.feedback = (feedback === data.feedback) ? 'ignore' : feedback;
    resources[index] = Object.assign(resources[index], data);
    this.setState({ resources });
    Service.updateFeedback({
      contentUrl: data.resource,
      contentCategory: this.props.data.category,
      feedback: data.feedback
    });
  }
  render() {
    const { resources } = this.state,
      { data: { category } } = this.props;
    return (
      <div className="sub-content">
        {
          resources && resources.map((d, i) => {
            return (
              <div className="resource-component" key={i}>
                <div className="doc-img" onClick={e => { this.onResourceClick(d) }}>
                  <img src={categoryIcon[category]} />
                </div>
                <div className="doc-content" onClick={e => { this.onResourceClick(d) }}>
                  <h4 className="heading">{d && d.title || category}</h4>
                  {(d && d.description) ? <p className="description">{d.description}</p> : null}
                </div>
                <div className={`content-feedback ${(d.feedback === "ignore") ? "hide" : "show"}`}>
                  <a className={`thumb-icon like${(d.feedback === "like") ? " selected" : ""}`} onClick={e => { this.updateFeedback(d, i, 'like') }} >
                    <span className="icon-thumb-up"></span>
                    {/* <FontAwesomeIcon icon={d.feedback === 'like' ? faThumbsUpFilled : faThumbsUp} /> */}
                  </a>
                  <a className={`thumb-icon dislike${(d.feedback === "dislike") ? " selected" : ""}`} onClick={e => { this.updateFeedback(d, i, 'dislike') }}>
                    <span className="icon-thumb-down"></span>
                    {/* <FontAwesomeIcon icon={d.feedback === 'dislike' ? faThumbsDownFilled : faThumbsDown} /> */}
                  </a>
                </div>
              </div>
            )
          })
        }
      </div>
    );
  }
}
