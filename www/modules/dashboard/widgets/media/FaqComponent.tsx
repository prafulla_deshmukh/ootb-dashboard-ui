import * as React from "react";
import * as ReactDOM from "react-dom";
import Service from "../../Service";
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faThumbsUp as faThumbsUpFilled, faThumbsDown as faThumbsDownFilled } from '@fortawesome/free-solid-svg-icons';
// import { faThumbsUp, faThumbsDown } from '@fortawesome/free-regular-svg-icons';

type rData = {
  title: string,
  resource: string
};

type Data = {
  category: string,
  resources: rData[]
};

export type IFaqProps = {
  data: Data;
  onMediaMsgClick: Function
};

type feedbackData = {
  title: string,
  resource: string,
  feedback: string
};
export type IFaqStates = {
  resources: feedbackData[]
};
export class FAQComponent extends React.Component<IFaqProps, IFaqStates> {
  constructor(props) {
    super(props);
    this.state = {
      resources: []
    };
  }

  onFaqClick(data) {
    this.props.onMediaMsgClick && this.props.onMediaMsgClick(Object.assign(data, { category: this.props.data && this.props.data.category }));
  }
  componentWillReceiveProps(nextProps) {
    const { resources } = nextProps.data;
    if (resources && resources.length > 0) {
      let resourcesData = [] as any;
      resourcesData = resources.map(data => {
        return {
          title: data.title,
          resource: data.resource,
          feedback: 'ignore'
        };
      });
      this.setState({
        resources: resourcesData
      })
    }
  }
  updateFeedback = (data, index, feedback) => {
    const { resources } = this.state;
    data.feedback = (feedback === data.feedback) ? 'ignore' : feedback;
    resources[index] = Object.assign(resources[index], data);
    this.setState({ resources });
    Service.updateFeedback({
      contentUrl: data.resource,
      contentCategory: this.props.data.category,
      feedback: data.feedback
    });
  }
  render() {
    const { resources } = this.state;
    return (
      <div className="sub-content">
        {
          <ul className="faq">
            {resources && resources.map((faq, index) => {
              return (
                <li key={faq.title}>
                  <span className="faq-title" onClick={e => { this.onFaqClick(faq) }}>{faq.title}</span>
                  <span className={`content-feedback ${(faq.feedback === "ignore") ? "hide" : "show"}`}>
                    <a className={`thumb-icon like${(faq.feedback === "like") ? " selected" : ""}`} onClick={e => { this.updateFeedback(faq, index, 'like') }} >
                    <span className="icon-thumb-up"></span>
                      {/* <FontAwesomeIcon icon={faq.feedback === 'like' ? faThumbsUpFilled : faThumbsUp} /> */}
                    </a>
                    <a className={`thumb-icon dislike${(faq.feedback === "dislike") ? " selected" : ""}`} onClick={e => { this.updateFeedback(faq, index, 'dislike') }}>
                    <span className="icon-thumb-down"></span>
                      {/* <FontAwesomeIcon icon={faq.feedback === 'dislike' ? faThumbsDownFilled : faThumbsDown} /> */}
                    </a>
                  </span>
                </li>
              )
            })}
          </ul>
        }
      </div>
    );
  }
}