import * as React from "react";
import { FormEvent } from "react";
import { WidgetPanel } from "bru-web-sdk/packages/bru-ui-dashboard";
import { ThreadDisplayComponent, BotMessage, MessageTypeRegistry } from "chatbot-ui-sdk";
import { ChatClientSocket } from "../../../../lib/chat-client";
import sendIcon from "../../../../theming/default/images/send-message-icon.png";
import * as Util from "../../../../lib/util";
import Config from "../../../../config";

import { session as SessionStore } from "store2";

const SEARCH_DELAY: number = 300,
  reconnectTimeout = 3000,
  reconnect = 5,
  disableTextboxFor = [{
    type: "feedback"
  }, {
    type: "typing"
  },
  {
    type: "conversation_end"
  }],
  invalidateMsgType = [{
    type: "conversation_end"
  }];

class ChatbotWidget extends React.Component<any, any> {
  defaultPlaceHolder = 'You can enter more details here too';
  chatClientSocket: any = null;
  reconnectCount: number = 0;

  state = {
    allowTyping: true,
    invalidated: false,
    status: 'loading',
    messages: [],
    inputText: "",
    placeholder: this.defaultPlaceHolder,
    busy: false
  }

  componentWillMount() {
    const chatClientOptions = {
      serverUrl: `${(Config.botApi.serverUrl || Config.apiServerUrl)}${(Config.botApi.path || Config.apiBasePath)}`,
      useSocket: true
    };
    this.chatClientSocket = new ChatClientSocket(chatClientOptions);
    this.chatClientSocket.connect();
  }
  componentDidUpdate() {
    // this.changeLayout();
  }

  componentDidMount() {
    const { context: { applianceInfo: { model: applianceModel, serial: applianceSerial }, problemCategory, description, model, serial } } = this.props,
      msgContent = {
        attributes: {
          applianceModel: applianceModel || model, applianceSerial: applianceSerial || serial, problemCategory, description
        }
      };
    this.chatClientSocket.on("connected", res => {
      const msg = {
        type: "hello",//this.connectEvent,
        user: SessionStore.get(Config.guidkey),
        channel: 'websocket',
        text: '',
        contentType: 'text',
        content: msgContent,
        agentCode: Util.getCookie(Config.agentCodeKey)
      };
      this.setState({
        messages: [],
        invalidated: false
      }, () => { this.chatClientSocket.send(msg); });
    });

    this.chatClientSocket.on("message", (e, res) => {
      const msg = res.data && JSON.parse(res.data);
      this.disabledInputBox(msg);
      if (msg.type == "conversation_end") {
        this.chatClientSocket.trigger('conversation_end', msg);
        return;
      }

      this.addToConversation(msg);
    });

    this.chatClientSocket.on("conversation_end", (e, res) => {
      // const { context: { resetActions } } = this.props,
      //   resetMsg = {
      //     type: "QUESTION", contentType: "options/select_one",
      //     content: {
      //       options: resetActions
      //     }
      //   }
      this.addToConversation("");
      this.chatClientSocket.disconnect('explicit');
      this.invalidateChat(!this.isInputDisable(res, invalidateMsgType));
    });

    this.chatClientSocket.on("closed", (e, res) => {
      this.invalidateChat(true);
      if (!(res.code && res.code === 1000) && this.reconnectCount < reconnect) {
        setTimeout(() => {
          console.log('RECONNECTING ATTEMPT: ', ++this.reconnectCount);
          this.chatClientSocket && this.chatClientSocket.connect()
        }, reconnectTimeout);
      }
    });

    this.chatClientSocket.on("socketError", function (res) {
      console.log("socket Errorres", res);
    });

    this.chatClientSocket.on("web_err", function (res) {
      console.log("web_err res", res);
    });
    this.changeLayout();

    /*setTimeout(() => {
      this.addToConversation({
        "sessionId":"b0912110-b9d3-11e9-90ce-59774ab40adb",
        "text":"Here is a possible solution:",
        "additionalInfo":{"hint":""},
        "type":"chat",
        "contentType":"solution",
        "content":{
          "solution": {"type":"text/html","html":"ATTACHMENT"},
          "hints":[
            {"type":"text/html","html":""}
          ],
          "parts":[]
        },
        "metadata":{
          "query":"",
          "params":{
            "applianceModel":"GTW220",
            "applianceSerial":"LL111111",
            "problemCategory":"Tub Stained / Dirty",
            "description":""
          },
          "treePath":[
            {"id":"8818987452754229656","time":1565265450346},
            {"id":"8818987452754229654","reply":"Tub Stained / Dirty","score":"0.99889994","selection":"Auto","time":1565265450354},
            {"id":"8818987452754230003","time":1565265450354},
            {"id":"8818987452754230000","score":"0.98725593","selection":"Auto","time":1565265450360},
            {"id":"8818987452754230010","time":1565265450360}
          ]
        },
        "action":"provideAnswer",
        "channel":"websocket",
        "sent_timestamp":1565265450362,
        "user":"2797e790-22b2-7afc-bc1b-64262a5ff98a",
        "to":"2797e790-22b2-7afc-bc1b-64262a5ff98a",
        "id":"b394c420-b9d3-11e9-90ce-59774ab40adb",
        "timestamp":362
      })
    }, 300);*/
  }

  invalidateChat = (flag) => {
    this.setState({
      invalidated: flag
    })
  }
  changeLayout = () => {
    this.props.layout.layout();
  }

  onActions(action, data) {
    const { onAction } = this.props;
    onAction && onAction(action, data);
  }

  change = Util.throttle((searchTerm) => {
    this.onActions("updateRecords/searchTerm", { searchTerm });
  }, SEARCH_DELAY);

  handleDetail = (message: BotMessage) => {
    this.props.onAction("updateRecords/drawerContent", message);
  }

  handleAction(selection: any, selectionLabel: string, message: BotMessage) {
    const action = selection.content && selection.content.options && selection.content.options[0],
      { context: { skipSearch, ejectActions, model, serial } } = this.props;

    if (action && action.value && ejectActions && ejectActions.includes(action.value)) {
      this.setState({
        messages: [],
        invalidated: false
      }, () => {
        const query = Util.makeQueryParams({ model, serial }),
          path = `/launch?${query}`;
        location.replace(path);
      });
      return;
    }
    const msg: BotMessage = {
      type: 'outgoing',
      source: 'user',// : 'bot',
      text: Util.escapeHtml(selectionLabel),
      contentType: 'text'
    };
    // this.addToConversation(msg);

    if (skipSearch && skipSearch.indexOf(message.type) == -1)
      this.change(selectionLabel);

    this.chatClientSocket.send(Object.assign({
      user: SessionStore.get(Config.guidkey),
      channel: 'websocket',
      agentCode: Util.getCookie(Config.agentCodeKey)
    }, selection))
  }

  renderMessages() {
    return <ThreadDisplayComponent messages={this.state.messages}
      onDetail={this.handleDetail}
      onAction={this.handleAction.bind(this)} />;
  }

  handleKeyAction = (e) => {
    const { keyCode } = e;
    e.stopPropagation();
    e.preventDefault();
    e.nativeEvent.stopImmediatePropagation();
    if (keyCode === 13) {
      this.sendMessage(e);
    }
  }

  sendMessage(event: FormEvent<HTMLFormElement>) {
    event.stopPropagation();
    event.preventDefault();
    event.nativeEvent.stopImmediatePropagation();
    if (!this.state.inputText) { return; }
    this.send(this.state.inputText)
    this.setState({ inputText: "" });
  }

  send(txt) {
    const message: BotMessage = {
      type: 'outgoing',
      source: 'user',// : 'bot',
      text: Util.escapeHtml(txt),
      contentType: 'text'
    };
    this.addToConversation(message);
    const userMsg = {
      type: 'message',
      text: txt,
      user: SessionStore.get(Config.guidkey),
      agentCode: Util.getCookie(Config.agentCodeKey),
      channel: 'socket',
      contentType: 'text'
    }

    this.chatClientSocket.send(userMsg);
  }

  onInputChange = (e) => {
    this.setState({ inputText: e.target.value }, () => {
      this.change(this.state.inputText);
    })
  }

  private normalizeForRender(isTyping: boolean) {
    this.setState({
      messages: this.state.messages
        .filter((m: BotMessage) => m.type !== 'typing')
        .map((m: BotMessage) => {
          if (!isTyping && m.action && m.action.includes('lookup')) {
            return { type: 'REMOVE' };
          }
          return m;
        })
        .filter((m: BotMessage) => m.type !== 'REMOVE')
    });
  }

  renderMessage(message: BotMessage) {
    this.setState({
      messages: [...this.state.messages, message]
    });
  }

  addToConversation(message: any) {
    const { invalidated } = this.state;
    this.normalizeForRender(message.type === 'typing');
    if (invalidated) {
      return false;
    }
    if (message.type !== 'typing') {
      const { channel, id, timestamp, text, textType, to, user, action, attachments } = message;
      if (message.text && message.text.length) {
        this.renderMessage({
          channel, timestamp, text, textType, to, user, action,
          source: message.source || 'bot', type: 'default', attachments
        })
      }
      if (message.content || message.attachments || (!message.text && message.errorCode && message.errorMessage)) {
        this.renderMessage(MessageTypeRegistry.transform(message));
      }
    } else {
      this.renderMessage(message);
    }
  }

  isInputDisable(msg, typeArr) {
    if (!msg) { return false; }
    return !typeArr.some(function (objVal) {
      return (msg.type && objVal.type === msg.type)
    });
  }

  disabledInputBox(msg) {
    const allowTyping = this.isInputDisable(msg, disableTextboxFor);
    this.setState({
      allowTyping,
      placeholder: allowTyping ? this.defaultPlaceHolder : "Keyboard input disabled"
    })
  }
  render() {
    const { Header, Content } = WidgetPanel;
    const { busy, placeholder, inputText, allowTyping } = this.state;
    return (
      <WidgetPanel {...this.props}>
        <Header>{this.props.title}</Header>
        <Content>
          <div className="chat-box">
            <section className="message-history-wrap">
              {this.renderMessages()}
            </section>
            <form className="message-input-wrap" id="message-input-wrap" onSubmit={e => !busy && this.sendMessage(e)}
            >
              <input
                onKeyUp={this.handleKeyAction}
                onChange={this.onInputChange}
                placeholder={placeholder}
                value={inputText}
                disabled={!allowTyping || busy} />
              <button type="submit" disabled={!allowTyping || busy} >
                <img src={sendIcon} alt="" />
              </button>
            </form>
          </div>
        </Content>
      </WidgetPanel>
    );
  }
};

const Info = {
  id: "ChatbotWidget",
  type: ChatbotWidget, // The widget we defined above
  name: "Chatbot", // The name of the widget
  description: "A simple widget"
};

export default {
  Info,
  ChatbotWidget: ChatbotWidget
};
