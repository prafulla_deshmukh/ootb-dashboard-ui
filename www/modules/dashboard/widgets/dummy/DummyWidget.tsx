import * as React from "react";
import { WidgetPanel } from "bru-web-sdk/packages/bru-ui-dashboard";

class DummyWidget extends React.Component {
  render() {
    const { Header, Content } = WidgetPanel;
    return (
      <WidgetPanel {...this.props}>
        <Header></Header>
        <Content>
        </Content>
      </WidgetPanel>
    );
  }
};

const Info = {
  id: "DummyWidget",
  type: DummyWidget, // The widget we defined above
  name: "", // The name of the widget
  description: ""
};

export default {
  Info,
  DummyWidget: DummyWidget
};
