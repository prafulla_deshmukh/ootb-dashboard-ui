import * as React from "react";
import Tabs from "../../../../components/Tabs/Tabs";
import {ModelNoTblComponent} from "./ModelNoWiseTable";
import {SerialNoTblComponent} from "./SerialNoWiseTable";
import {ApplianceInfoType} from "../../View";

export type IIssueHistoryProps = {
  dataModel: any;
  dataSerial: any;
  channel: string;
  layout: Function;
  // nextPage: Function;
  // previousPage: Function;
  changeTab: Function;
  applianceInfo: ApplianceInfoType
}

export type IIssueHistoryStates = {
};

export class IssueHistoryComponent extends React.Component<IIssueHistoryProps, IIssueHistoryStates> {
  componentDidMount(): void {
    this.props.layout();
  }

  changeTab = (tabIndex: number, label: string): void => {
    this.props.layout();
    this.props.changeTab(tabIndex, label);
  }

  /** renders the component */
  render() {
    const {applianceInfo: {model, serial}, dataModel, dataSerial, channel} = this.props;
    return (
      <div className="issue-history-component">
        <Tabs change={this.changeTab} defaultTab={(channel === "/issue-history-by-model" ? 1 : 0)}>
          <div data-label={serial || "Serial"} data-title={`Serial${serial ? `: ${serial}` : ''}`}>
            <SerialNoTblComponent layout={() => {
                this.props.layout();
              }} data={dataSerial}
              // previousPage={this.props.previousPage}
              // nextPage={this.props.nextPage}
              />
          </div>
          <div data-label={model || "Model"} data-title={`Model${model ? `: ${model}` : ''}`}>
            <ModelNoTblComponent data={dataModel} />
          </div>
        </Tabs>
      </div>
    );
  }
};