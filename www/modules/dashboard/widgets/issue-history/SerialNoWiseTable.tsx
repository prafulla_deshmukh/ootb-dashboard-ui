import * as React from "react";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle,faCheckCircle, faExclamationCircle } from '@fortawesome/free-solid-svg-icons';

type StatusInfo = {
  status: "ERROR" | "OK";
  message: string;
}

type Content = {
  id?: number;
  created: string;
  problem: string;
  severity: "MAJOR" | "CRITICAL" | "WARNING" | "OK";
  action: string;
  status: string,
  jobDescription: string,
  parts: string,
  modelNumber?: string,
  actionTaken?: string,
  callNumber?: string,
  invoice?: string,
  serialNumber?: string
  // "status-info": StatusInfo;
};

type SerialNoData = {
  // pageNumber: number;
  // pageSize: number;
  // totalPages: number;
  // totalElements: number;
  content: Content[];
}

type ISerialNoTblComponentProps = {
  // pageSize?: number;
  data: SerialNoData;
  // nextPage: Function;
  // previousPage: Function;
  layout: Function;
};

export class SerialNoTblComponent extends React.Component<ISerialNoTblComponentProps, any> {

  static defaultProps = {
    // pageSize: 5,
    data: {
      content: []
    }
  };

  constructor(props: any) {
    super(props);

    this.state = {
      data: this.props.data
    };
  }

  componentDidUpdate() {
    this.props.layout();
  }

  formatDate(ts) {
    let d = new Date(ts);
    return (d.getUTCMonth() + 1) + "/" + d.getUTCDate();
  }

  renderSeverityComponent(severity) {
    let s = severity || "low";
    return (
      <div className={`severity-bar ${ s.toLowerCase() }`}
          title={s.toUpperCase()}></div>
    );
  }
  
  render() {
    const {data: {content = [],
        // pageNumber, totalPages
      },
      // previousPage, nextPage
    } = this.props,
    issueRows = content.sort((a: any, b: any) => b.created - a.created).map((issue: Content, i: number) => {
      // const {"status-info": {status}} = issue,
      //   statusClass = status === "OK"
      //     ? "check"
      //     : "alert",
      const severity = this.renderSeverityComponent(issue.severity);

      return (
        // <tr key={`${issue.id}_${i}`} className="issue">
        //   <td className="date">{this.formatDate(issue.created) || " "}</td>
        //   <td className="desc lg">{issue.problem || ""}</td>
        //   <td className="severity">{severity || " "}</td>
        //   <td className="action lg">{issue.action || ""}</td>
        //   <td className="status" title={issue.status || "NA"}>
        //     <span>
        //       {issue.status || "NA"}
        //     </span>
        //     {/* <FontAwesomeIcon className={`icon`} size="1x" icon={faInfoCircle} /> */}
        //   </td>
        // </tr>
        //for ie10
        <li key={`${issue.id}_${i}`} className="issue">
          <span className="date">{this.formatDate(issue.created) || " "}</span>
          <span className="problem">{issue.problem || ""}</span>
          <span className="desc">{issue.jobDescription || " "}</span>
          <span className="parts">{issue.parts || ""}</span>
          {/* <span className="status">{issue.status || "NA"}</span> */}
        </li>
      );
    });
    return (
      <div>
        {/* <div className="table-container">
          <table className="data-table issue-history fixed_header">
            <thead>
              <tr>
                <th>Date</th>
                <th className="lg">Problem</th>
                <th>Severity</th>
                <th className="lg">Action</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              {
                (content.length === 0
                  ? (<tr>
                      <td colSpan={5} className="empty">No Records</td>
                  </tr>)
                  : issueRows)
              }
            </tbody>
          </table>
        </div> */}
        <div className="table-container">
          <ul>
            <li className="fixed_header">
              <span>Date</span>
              <span>Problem</span>
              <span>Job Desc</span>
              <span>Parts</span>
              {/* <span>Status</span> */}
            </li>
            <ul className="issue-content">
              {
                (content.length === 0
                  ? (<li>
                    <span className="empty">No Records</span>
                  </li>)
                  : issueRows)
              }
            </ul>
          </ul>
        </div>
        <div className="footer">
          {/* <span className="actions">
            <button onClick={() => {
              previousPage(pageNumber - 1);
            }}
              disabled={pageNumber === 0 || content.length === 0} className="primary previous">&#171;</button>
            <button onClick={() => {
              nextPage(pageNumber + 1);
            }}
              disabled={pageNumber === totalPages - 1 || content.length === 0} className="primary next">&#187;</button>
          </span> */}
        </div>
      </div>
    );
  }
};
