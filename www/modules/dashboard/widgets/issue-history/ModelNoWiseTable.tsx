import * as React from "react";

type Issues = {
  problem: string;
  solution: string;
  stc_percentage: number;
};

type Data = {
  model: string;
  issues: Issues[];
}

type IModelNoTblCompProps = {
  data: Data;
}

type IModelNoTblCompStates = {
}

export class ModelNoTblComponent extends React.Component<IModelNoTblCompProps, IModelNoTblCompStates> {
  static defaultProps = {
    data: {
      issues: []
    }
  };
  render() {
    const {data: {issues = []}} = this.props,
        issueRows = issues.map((issue, i) => {
          return (
            // <tr key={`${i}`} className="issue">
            //   <td className="lg">{issue.problem || " "}</td>
            //   {/* <td className="lg">{issue.solution || ""}</td>
            //   <td>{`${issue.stc_percentage}%` || " "}</td> */}
            // </tr>
            // for ie10
            <li key={`${i}`} className="issue">
              <span className="desc"> {issue.problem || " "}</span>
            </li>
          );
        });
    return (
      // for ie10
      <div className="table-container">
        <ul>
          <li className="fixed_header">
            <span>Top 5 Issues</span>
          </li>
          <ul className="issue-content">
            {
              (issueRows.length === 0
                ? (<li>
                  <span className="empty">No Records</span>
                </li>)
                : issueRows)
            }
          </ul>
        </ul>
        {/* <table className="data-table issue-history fixed_header model_no">
          <thead>
            <tr>
              <th className="lg">Top 5 Issues</th>
              {/* <th className="lg">Solution</th>
              <th>STC</th> 
            </tr>
          </thead>
          <tbody>
            {
              (issueRows.length === 0
                ? (<tr>
                    <td colSpan={5} className="empty">No Records</td>
                </tr>)
                : issueRows)
            }
          </tbody>
        </table> */}
      </div>
    );
  }
};