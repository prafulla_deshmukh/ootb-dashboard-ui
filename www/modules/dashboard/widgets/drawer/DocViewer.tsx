import React, { Component } from 'react';
import { Document, Page, pdfjs } from 'react-pdf';

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

const highlightPattern = (text, pattern) => {
  const splitText = text.split(pattern);
  if (splitText.length <= 1) {
    return text;
  }

  const matches = text.match(pattern);

  return splitText.reduce((arr, element, index) => (matches[index] ? [
    ...arr,
    element,
    <mark>
      {matches[index]}
    </mark>,
  ] : [...arr, element]), []);
};

type IDocViewerProps = {
  file: Object,
  searchTerm: string,
  pageNumber: number,
  pdfMetaData?: any
};

type IDocViewerState = {
  searchTerm: string,
  pageNumber: number,
  file: Object,
  numPages: null | number
};

export class DocViewer extends React.Component<IDocViewerProps, IDocViewerState> {
  state = {
    searchTerm: this.props.searchTerm,
    pageNumber: this.props.pageNumber,
    file: this.props.file,
    numPages: null
  }

  makeTextRenderer = searchText => textItem => highlightPattern(textItem.str, searchText);

  onDocumentLoadSuccess = (document) => {
    const {numPages} = document;
    this.setState({
      pageNumber: 1,
      numPages
    });
  };
  
  componentWillReceiveProps(nextProps) {
    const { searchTerm, pageNumber, file } = nextProps;
    let stateData = {};
    if (searchTerm !== this.state.searchTerm) {
      stateData["searchTerm"] = searchTerm;
    }
    if (pageNumber !== this.state.pageNumber) {
      stateData["pageNumber"] = searchTerm;
    }
    if (file !== this.state.file) {
      stateData["file"] = file;
    }

    if(stateData) {
      this.setState(stateData);
    }
  }

  render() {
    const { searchTerm, pageNumber, file, numPages} = this.state;
    return (
        <Document
          file={file}
          onLoadSuccess={this.onDocumentLoadSuccess}
        >
        {Array.from(
          new Array(numPages),
          (el, index) => (
            <Page
            pageNumber={index + 1}
            scale={0.8}
            key={`page_${index + 1}`}
            customTextRenderer={this.makeTextRenderer(searchTerm)}
          />
          ),
        )}
          
        </Document>
    );
  }
}