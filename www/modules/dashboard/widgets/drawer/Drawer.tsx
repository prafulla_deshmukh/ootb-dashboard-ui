import * as React from "react";
import * as ReactDOM from "react-dom";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes, faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons';
import { WebpagePreviewComponent, YoutubeVideoComponent } from "chatbot-ui-sdk";
import Config from "../../../../config";
import { DocViewer } from "./DocViewer";
import * as Util from "../../../../lib/util"

interface IDrawerProps {
  sidePanel: boolean,
  toggleDrawer: Function,
  popOut: Function,
  selectedMessage: Object,
  searchTerm: string
};

interface IDrawerStates {
  renderMedia: boolean
};

const renderDelay = 700;

export class Drawer extends React.Component<IDrawerProps, IDrawerStates> {
  static defaultProps = {
    sidePanel: false,
    selectedMessage: {}
  }

  state = {
    renderMedia: false
  }

  renderMediaDetails() {
    //@ts-ignore
    const {selectedMessage:{category, text, resource, title}, searchTerm} = this.props;

    switch (category) {
      case 'Manual':
        const file = {
          url: resource
        }
        return <DocViewer searchTerm={searchTerm} file={{
          url: resource
        }} pageNumber={1}></DocViewer>
        break;
      case 'Video':
        return <YoutubeVideoComponent baseUrl={resource} />
           //metaData={`${Config.apiServerUrl}/download?file=${encodeURIComponent(resource)}`}
          break;
      case 'FAQ':
        return (<WebpagePreviewComponent highlight="" href={resource} />);
        break;
      default:
        return text ? <div>{text}</div> : 'No Content';
    };
  }
  componentDidMount() {
    this.delayMediaComponent();
  }
  delayMediaComponent = Util.throttle(() => {
    this.setState({
      renderMedia: true
    })
  }, renderDelay);

  render() {
    //@ts-ignore
    const { title, category } = this.props.selectedMessage,
      { renderMedia } = this.state;
    return (
      <div className="drawer">
        <div className="dr-header">
          <span className="title">{title || category}</span>
          <div onClick={() => {
            this.props.popOut();
          }} className="window-icon">
            <FontAwesomeIcon icon={faExternalLinkAlt} />
          </div>
          <div onClick={() => {
            this.props.toggleDrawer();
          }} className="close-icon">
            <FontAwesomeIcon icon={faTimes} />
          </div>
        </div>
        <div className="dr-content">
          {renderMedia && this.renderMediaDetails()}
        </div>
      </div>
    );
  }
}