import * as React from "react";
import dashboardFactory from "./config";

import Dashboard from "../../components/dashboard";
import Service, { MediaSearchType } from "./Service";
import * as Util from "../../lib/util";
import Config from "../../config";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';

import {session as SessionStore} from "store2";

export type IDashboardProps = {
  match: any,
  location: any
};
export type ApplianceInfoType = {
  model?: string,
  serial?: string,
  type?: string,

}
export type IDashboardStates = {
  dashboardConfig: object,
  applianceInfo: ApplianceInfoType,
  searchTerm: string,
  loading: boolean,
  selectedMsg: any,
  problemCategory?: string,
  description?: string
};

const EMPTY_DASH = {
  name: "Simple",
  datalayer: {
    name: "None",
    channels: []
  },
  widgets: []
};

const OTHER_PROBLEMS = ["Other Not Listed"].map(item => item.toLowerCase());

const WidgetAPIResources = [
  { category: "FAQ", channel: "/faq" },
  { category: "Manual", channel: "/manual" },
  { category: "Video", channel: "/video" },
  { category: null, channel: "/issue-history-by-model", resource: "fetchSTCIssues", skipInitialLoad: true }
]

const openNative = [
  { category: "FAQ" },
  { category: "Manual" }
],
  contentTypeMap = {
    html: "FAQ",
    pdf: "Manual",
    "video/youtube": "Video"
  },
  skipSearch = ["feedback"],
  ejectActions = [
    '__reset_chat__'
  ];
  // resetActions = [
  //   { label: "Start Over", value: "__reset_chat__" }
  // ];

export class DashboardView extends React.Component<IDashboardProps, IDashboardStates> {
  dashboardRef: any;
  state = {
    applianceInfo: {},
    problemCategory: "",
    description: "",
    searchTerm: "",
    dashboardConfig: EMPTY_DASH,
    loading: false,
    selectedMsg: null
  };

  getSearchTearm(searchStr = "") {
    const { problemCategory = "", description = "" } = this.state,
      descValue = description && description.trim(),
      searchProblem = (OTHER_PROBLEMS.indexOf(problemCategory.toLowerCase()) !== -1) ?
        descValue :
        `${problemCategory}${descValue ? `#$#${descValue}` : ""}`,
      searchTerm = `${searchProblem}${descValue ? ` ${searchStr}` : `${searchStr ? `#$#${searchStr}` : ""}`}`.trim();

    this.setState({ searchTerm });
    return searchTerm;
  }

  componentWillMount() {
    const { location: { state }, match: { params } } = this.props,
      { model = "", serial = "" } = params || state || {},
      { description = "", problemCategory = "" } = Util.getQueryParams(location.search) || {};

    this.setState({
      dashboardConfig: dashboardFactory({}),
      applianceInfo: {
        model,
        serial
      },
      problemCategory,
      description
    });
  }
  componentDidMount() {
    const { match: { params } } = this.props,
    //@ts-ignore
     {problemCategory, description, applianceInfo: {model, serial}} = this.state;

    this.getAppliance();
    this.getAllWidgetData(this.getSearchTearm(), true);
    this.getTopIssues();
    this.getIssueHistory();
    const sysLogData = {
      problemCategory,
      description,
      model,
      serial,
      systemOs: Util.sysDetails.OS,
      systemBrowser: Util.sysDetails.browser,
      browserVersion: Util.sysDetails.browser.version,
      screenResolution: window.screen ? `${window.screen.width}X${window.screen.height}` : "0X0",
      addInfo: {
        userAgent: navigator && navigator.userAgent,
        appVersion: navigator && navigator.appVersion,
        platform: navigator && navigator.platform
      },
      message: "System Info",
      targetType: "SystemInfo",
      event: "sysInfo",
      target: "SystemInfo"
    }
    Service.log(sysLogData);
  }
  getAllWidgetData(searchTerm: string = "", initialLoad: boolean = false) {
    WidgetAPIResources.forEach(wid => {
      if (!(initialLoad && wid.skipInitialLoad)) {
        wid.category ? this.getResources(wid.category, wid.channel, searchTerm) : wid.resource && this[wid.resource] && this[wid.resource](wid.channel, searchTerm);
      }
    })
  }
  fetchSTCIssues(channel: string, searchTerm: string = "") {
    // @ts-ignore
    const { applianceInfo: { model = "" } } = this.state;
    if (model) {
      Service.fetchStcIssues({ model: model, searchTerm: searchTerm }).then(res => {
        this.dashboardRef && this.dashboardRef.DataLayer.publish(channel, res);
      }).catch(errRes => {
      });
    }
  }
  // getApplianceFallBack = Util.throttle((resolve, reject, applianceInfo) => {
  //   this.setState({
  //     applianceInfo,
  //     loading: false,
  //     dashboardConfig: dashboardFactory(applianceInfo)
  //   });
  //   resolve(applianceInfo);
  // }, 5000);

  getAppliance() {
    const { match: { params }, location: { state } } = this.props;

    const applianceInfo = {
      model: state && state.model,
      serial: state && state.serial,
      type: ""
    }
    Service.fetchAppliance(params.model, params.serial).then(data => {
      this.setState({
        applianceInfo: {
          model: data.model,
          serial: data.serial,
          type: data.type
        }
      });
      this.dashboardRef && this.dashboardRef.DataLayer.publish("/appliance", data);
    }).catch(errRes => {
      this.dashboardRef && this.dashboardRef.DataLayer.publish("/appliance", applianceInfo);
    })
  }

  getTopIssues() {
    const { match: { params } } = this.props;
    Service.fetchTopIssues(params.model).then(data => {
      this.dashboardRef && this.dashboardRef.DataLayer.publish("/top-issues", data);
    }).catch(errRes => {
    })
  }

  getResources(category, channelName, searchTerm) {
    //@ts-ignore
    const { applianceInfo: { model = "", type = "" } } = this.state;
    const { location: { state }, match: { params } } = this.props,
      { model: modelNumber = "" } = state || params || {};
    const resourceData: MediaSearchType = {
      model: model || modelNumber,
      type: type,
      category: category,
      userId: SessionStore.get(Config.guidkey),
      searchTerm: searchTerm
    }
    if (model) {
      Service.fetchResources(resourceData).then(data => {
        this.dashboardRef && this.dashboardRef.DataLayer.publish(channelName, data);
      }).catch(errRes => {
      })
    }
  }
  onWidgetAction = (widgetId, action, data) => {
    const act = widgetId + "/" + action;
    switch (act) {
      // case "MediaWidget/updateRecords/serial":
      //   // this.getIssueHistory(data);
      //   this.getIssueHistory();
      //   break;
      case "MediaWidget/updateRecords/searchTerm":
        this.fetchSTCIssues("/issue-history-by-model");
        break;
      case "Chatbot/updateRecords/drawerContent":
        this.setState({ selectedMsg: data });
        break;
      case "MediaWidget/updateRecords/drawerContent":
        this.setState({ selectedMsg: null });
        break;
      case "Chatbot/updateRecords/searchTerm":
        this.getAllWidgetData(this.getSearchTearm(data.searchTerm));
        break;
      default:
        console.log("Unhandled Widget Action", widgetId, action, data);
    }
  }
  // getIssueHistory(data) {
  getIssueHistory() {
    //@ts-ignore
    const { applianceInfo: { model = "", type = "", serial = "" } } = this.state;
    const resourceData = {
      model,
      type,
      serial,
      // ...data
    }
    if (model.length > 0) {
      Service.fetchIssueHistoryBySerial(resourceData).then(res => {
        this.dashboardRef && this.dashboardRef.DataLayer.publish("/issue-history-by-serial", res);
      }).catch(errRes => {
      });
    }
  }
  onDashboardChange(datalayer) {
  }
  render() {
    const { dashboardConfig, applianceInfo, searchTerm, loading, selectedMsg, problemCategory, description } = this.state,
      { location: { state } } = this.props,
      ctx = Object.assign({},
        { applianceInfo, searchTerm }, state,
        { openNative, selectedMsg, contentTypeMap, skipSearch, ejectActions },
        { problemCategory, description }
      );
    return (loading) ? (
      <div className="dashboard-loading">
        <FontAwesomeIcon icon={faSpinner} spin={true} pulse={true} size="6x" />
        <p className="loading-txt">Loading...</p>
      </div>
    ) : (
        <div className="dashboard-view">
          <div className="content">
            <Dashboard
              ref={x => this.dashboardRef = x}
              context={ctx}
              onChange={this.onDashboardChange}
              onWidgetAction={this.onWidgetAction}
              config={dashboardConfig} />
          </div>
        </div>
      );
  }
}
