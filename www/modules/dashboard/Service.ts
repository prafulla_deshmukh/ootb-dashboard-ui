/* global Promise setTimeout */
import ApiClient from "../../lib/api-client";
import Config from "../../config"
import * as Util from "../../lib/util";
import { session as SessionStore } from "store2";

const { instance: Api, asJson: resAsJson, asQueryParameters } = ApiClient;

type ParamsForSerial = {
  model: string;
  serial: string;
  // pageNumber: number;
  // pageSize: number;
}

export type STCParamsType = {
  model: string;
  searchTerm?: string;
}
type PromisesWithAbort = {
  stc: any,
  faq: any,
  manual: any,
  Video: any
}

let promisesWithAbort: PromisesWithAbort = {
  stc: null,
  faq: null,
  manual: null,
  Video: null
}

export type MediaSearchType = {
  model: string,
  type: string,
  category: string,
  userId: string,
  searchTerm?: string
}

export default {
  fetchAppliance(model: string, serial: string): any {
    const paramStr = asQueryParameters({
      model: model,
      serial: serial
    });
    return Api.get(`/appliance-info?${paramStr}`).then(resAsJson)
      .then(appliance => {
        return appliance;
      });
  },
  fetchTopIssues(model: string): any {
    const paramStr = asQueryParameters({
      model: model
    });
    return Api.get(`/appliance-top-issues?${paramStr}`).then(resAsJson)
      .then(topIssues => {
        return topIssues;
      })
  },
  fetchResources(data: MediaSearchType): any {
    let { category } = data;
    category = category.toLocaleLowerCase();

    if (promisesWithAbort[category] && promisesWithAbort[category]._abort) {
      promisesWithAbort[category]._abort();
    }
    promisesWithAbort[category] = Api.post("/appliance-document-search", {
      body: JSON.stringify(data)
    })
    return promisesWithAbort[category].then(resAsJson)
      .then(data => {
        return data;
      })
  },
  fetchStcIssues(params: STCParamsType) {
    const queryString = asQueryParameters({
      model: params.model,
      searchTerm: params.searchTerm,
    });
    if (promisesWithAbort.stc && promisesWithAbort.stc._abort) {
      promisesWithAbort.stc._abort();
    }
    promisesWithAbort.stc = Api.get(`/appliance-stc-issues?${queryString}`);
    return promisesWithAbort.stc.then(resAsJson);
  },
  fetchIssueHistoryBySerial(params: ParamsForSerial): any {
    const queryString = asQueryParameters({
      model: params.model,
      serial: params.serial,
      // pageNumber: params.pageNumber,
      // pageSize: params.pageSize || 5
    });
    return Api.get(`/appliance-service-history?${queryString}`)
      .then(resAsJson);
  },
  modelImageConfig: {
    "GTW220": 4,
    "GTW330": 5,
    "GTW335": 61,
    "HTW200": 5,
    "HTW240": 5
  },
  getModelImgs(modelFamily) {
    const model = modelFamily && modelFamily.toUpperCase(),
      imgCount = this.modelImageConfig && this.modelImageConfig[model];
    let imgArr: string[] = [],
      thumbnailArr: string[] = [];

    if (imgCount) {
      for (let i = 0; i < imgCount; i++) {
        imgArr = [...imgArr, `${modelFamily}/${modelFamily}_${i}`];
        thumbnailArr = [...thumbnailArr, `${modelFamily}/${modelFamily}_thumbnail_${i}`];
      }
      imgArr = [...imgArr, `${modelFamily}/${modelFamily}`]
      thumbnailArr = [...thumbnailArr, `${modelFamily}/${modelFamily}_thumbnail`]
    }
    return {
      modelImages: imgArr,
      thumbnailImages: thumbnailArr
    };
  },
  log(data = {}) {
    const dataObj = Object.assign(data, {
      agentCode: Util.getCookie(Config.agentCodeKey),
      sessionId: SessionStore.get(Config.guidkey),
      eventType: Config.loggerType, timeStamp: new Date().getTime()
    })
    Api.post("/log", {
      body: JSON.stringify(dataObj)
    })
  },
  updateFeedback(data = {}): any {
    const dataObj = Object.assign(data, { sessionId: SessionStore.get(Config.guidkey) })
    Api.post("/feedback-service/contentfeedback/", {
      body: JSON.stringify(dataObj)
    })
  },
};
