import routes from "./routes";
import {DashboardView} from "./View";

export {
  routes,
  DashboardView
};
