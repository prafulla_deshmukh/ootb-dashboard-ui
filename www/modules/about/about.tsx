import * as React from "react";
import * as ReactDOM from "react-dom";
import * as Config from "../../config";

export type IAboutProps = {};

export type IAboutStates = {};

export class About extends React.Component<IAboutProps, IAboutStates> {
  render() {
    return (
        <div className="about-view">
          <div className="content">
            <h2>{Config.appname}</h2>
            <h6>Version {Config.appversion}</h6>
            <button className="primary inline" onClick={_ => history.go(-1)}>OK</button>
          </div>
        </div>
      );
  }
}
