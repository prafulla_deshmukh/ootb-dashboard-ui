// /* global test expect*/
import * as React from "react";
import {shallow} from "enzyme";
import {About} from "../";

test("Checking for About Component", () => {
  const wrapper = shallow(<About />);
  expect(wrapper.find("button").hasClass("inline")).toEqual(true);
});
