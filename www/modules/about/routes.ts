import {About} from "./about";

export default [
  {
    path: "/about",
    component: About,
    exact: true,
    name: "About",
    auth: true
  }
];
