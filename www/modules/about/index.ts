import routes from "./routes";
import {About} from "./about";

export {
  routes,
  About
};
