/* global WebSocket */
import { EventEmitter } from "./util";

export class ChatClientSocket extends EventEmitter {
  private socket: WebSocket;
  private serverUrl: string;
  private useSocket: boolean;

  constructor(options) {
    super();
    this.serverUrl= options.serverUrl;
    this.useSocket= options.useSocket;
  }
  private connect() {
    if(this.useSocket) {
      this.connectWebsocket();
    } else {

    }
  };

  private connectWebsocket() {
    const url = (this.serverUrl.startsWith('https') ? 'wss' : 'ws') + this.serverUrl.replace(/^https?/, '');
    try {
      this.disconnect();
      this.socket = new WebSocket(url);
      this.intiSocketListenetrs();
    } catch {

    }
  }
  private intiSocketListenetrs(){
    this.socket.onopen = event => {
      this.trigger("connected", event);
    };

    this.socket.onerror = event => {
      this.trigger("socketError", event);
    };

    this.socket.onclose = event => {
      this.trigger("closed", event);
    };

    this.socket.onmessage = event => {
      this.trigger("message", event);
    };
  }
  private send(message) {
    if (this.useSocket) {
      this.sendMsgSocket(message);
    } else {
      //this.webhook(message);
    }
  }
  disconnect(reason: string = "closed") {
    if (this.isConnected()) {
      this.socket.close(1000, reason);
    }
  };
  // initialize(info) {
  //   this.info = info || { user: `user_${new Date().getTime()}`, channel: "websocket" };
  // };
  isConnected() {
    /*
     * 0 CONNECTING Socket has been created. The connection is not yet open.
     * 1 OPEN The connection is open and ready to communicate.
     * 2 CLOSING The connection is in the process of closing.
     * 3 CLOSED The connection is closed or couldn't be opened.
    */
    return this.socket && this.socket.readyState === 1;
  };
  sendMsgSocket(message) {
    if (this.isConnected()) {
      this.socket.send(JSON.stringify(message));
    } 
  };
  onMessage(handler) {
    return this.on("message", (evt, msg) => {
      handler(msg);
    });
  };
  onError(handler) {
    return this.on("error", (evt, err) => {
      handler(err);
    });
  }
}