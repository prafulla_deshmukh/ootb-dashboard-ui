export class EventEmitter {
  private eventHandlers: object;

  constructor() {
    this.eventHandlers = {}
  }

  on(event, handler) {
    const handlers = this.eventHandlers[event] || (this.eventHandlers[event] = []);
    handlers.push(handler);
    return {
      dispose() {
        const index = handlers.indexOf(handler);
        if (index !== -1) {
          handlers.splice(index, 1);
        }
      }
    };
  }
  once(event, handler) {
    let subs;
    subs = this.on(event, (...args) => {
      subs.dispose();
      handler(event, ...args);
    });
  }
  trigger(event, ...args) {
    let handlers = this.eventHandlers[event] || [];
    handlers.forEach(h => {
      h(event, ...args);
    });
  }
}

// const EventEmitterProto: any = {
//   on(event, handler) {
//     const handlers = this.eventHandlers[event] || (this.eventHandlers[event] = []);
//     handlers.push(handler);
//     return {
//       dispose() {
//         const index = handlers.indexOf(handler);
//         if(index !== -1) {
//           handlers.splice(index, 1);
//         }
//       }
//     };
//   },
//   once(event, handler) {
//     let subs;
//     subs = this.on(event, (...args) => {
//       subs.dispose();
//       handler(event, ...args);
//     });
//   },
//   emit(event, ...args) {
//     let handlers = this.eventHandlers[event] || [];
//     handlers.forEach(h => {
//       h(event, ...args);
//     });
//   }
// };

// export function createEventEmitter() {
//   return Object.create(EventEmitterProto, {
//     eventHandlers: {
//       value: {}
//     }
//   });
// };

export function throttle(callback: Function, delay: number = 500) {
  let timeoutId;
  return function (...args) {
    if (timeoutId) {
      window.clearTimeout(timeoutId);
    }
    timeoutId = setTimeout(() => {
      callback(...args);
    }, delay);
  };
};

export function pad(value: number, padding: string = "00") {
  const p = padding.substring(0, padding.length - (value + "").length);
  return p + value;
};

export function parseTemperature(value: number = 0) {
  let hex: string = value.toString(16).toUpperCase(),
    lowByte = parseInt(hex.substring(2), 16),
    highByte = parseInt(hex.substring(0, 2), 16);
  if (lowByte === 0xff) {
    return - (highByte ^ 0xff);
  } else if (lowByte === 0) {
    return highByte;
  }
  return null;
};

export function formatDate(date: Date, format: string, utc: boolean = false) {
  const regexpDATEFORMAT = /yyyy|yy|M+|d+|HH|H|hh|h|mm|m|ss|s|a|ww|w/g,
    getFullYear = utc ? "getUTCFullYear" : "getFullYear",
    getYear = utc ? "getUTCYear" : "getYear",
    getMonth = utc ? "getUTCMonth" : "getMonth",
    getDate = utc ? "getUTCDate" : "getDate",
    getHours = utc ? "getUTCHours" : "getHours",
    getMinutes = utc ? "getUTCMinutes" : "getMinutes",
    getSeconds = utc ? "getUTCSeconds" : "getSeconds",
    getMilliseconds = utc ? "getUTCMilliseconds" : "getMilliseconds",
    setHours = utc ? "setUTCHours" : "setHours",
    setDate = utc ? "setUTCDate" : "setDate",
    getDay = utc ? "getUTCDay" : "getDay",

    padLeft = (str: string, max: number, char = " ") => {
      let len = max - str.length;
      if (len < 0) {
        return str;
      }
      while (len--) {
        str = char + str;
      }
      return str;
    },
    padZeros = (value: number, count = 2) => {
      return padLeft(value.toString(), count, "0");
    };

  let half = false;

  if (format && format[0] === "!") {
    half = true;
    format = format.substring(1);
  }

  if (typeof (date) === "number") {
    date = new Date(date);
  }

  if (!format) {
    return date[getFullYear]() + "-" +
      padZeros(date[getMonth]() + 1) + "-" +
      padZeros(date[getDate]()) + "T" +
      padZeros(date[getHours]()) + ":" +
      padZeros(date[getMinutes]()) + ":" +
      padZeros(date[getSeconds]()) + "." +
      padZeros(date[getMilliseconds](), 3) + "Z";
  }

  let h = date[getHours]();

  if (half) {
    if (h >= 12)
      h -= 12;
  }

  return format.replace(regexpDATEFORMAT, function (key) {
    let tmp: any;
    switch (key) {
      case "yyyy":
        return date[getFullYear]();
      case "yy":
        return date[getYear]();
      case "MM":
        return padZeros(date[getMonth]() + 1);
      case "M":
        return (date[getMonth]() + 1);
      case "dd":
        return padZeros(date[getDate]());
      case "d":
        return date[getDate]();
      case "HH":
      case "hh":
        return padZeros(h);
      case "H":
      case "h":
        return date[getHours]();
      case "mm":
        return padZeros(date[getMinutes]());
      case "m":
        return date[getMinutes]();
      case "ss":
        return padZeros(date.getSeconds());
      case "s":
        return date[getSeconds]();
      case "w":
      case "ww":
        tmp = new Date(+date);
        tmp[setHours](0, 0, 0);
        tmp[setDate](tmp[getDate]() + 4 - (tmp[getDay]() || 7));
        const dateVal: any = new Date(tmp[getFullYear](), 0, 1);
        tmp = Math.ceil((((tmp - dateVal) / 8.64e7) + 1) / 7);
        return key === "ww" ? padLeft(tmp.toString(), 2, "0") : tmp;
      case "a":
        return date[getHours]() >= 12 ? "PM" : "AM";
    }
  }) + (utc ? " UTC" : "");
};


export function getObjectPath(object: any, path: any): any {
  let curr: any = object;
  path.split('.').forEach(p => {
    if (curr && typeof (curr) === 'object') {
      curr = curr[p];
    }
  });
  return curr || undefined;
};

export function escapeHtml(unsafe: string): string {
  return unsafe
    .replace(/&/g, "&amp;")
    .replace(/</g, "&lt;")
    .replace(/>/g, "&gt;")
    .replace(/"/g, "&quot;")
    .replace(/'/g, "&#039;")
    ;
};

export function setCookie(cname, cvalue, exdays) {
  const d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  const expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
};

export function getCookie(cname: string) {
  const name = cname + "=",
    decodedCookie = decodeURIComponent(document.cookie),
    ca = decodedCookie.split(';');
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
};

export function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
};

export function getQueryParams(query) {
  return query
    ? (/^[?#]/.test(query) ? query.slice(1) : query)
      .split('&')
      .reduce((params, param) => {
        let [key, value] = param.split('=');
        params[key] = value ? decodeURIComponent(value.replace(/\+/g, ' ')) : '';
        return params;
      }, {}
      )
    : {}
};

export function makeQueryParams(object) {
  return Object.keys(object).map(key => key + '=' + object[key]).join('&');
}

export const IE = (function () {
  "use strict";

  var ret, isTheBrowser,
    actualVersion,
    jscriptMap, jscriptVersion;

  isTheBrowser = false;
  jscriptMap = {
    "5.5": "5.5",
    "5.6": "6",
    "5.7": "7",
    "5.8": "8",
    "9": "9",
    "10": "10"
  };
  jscriptVersion = new Function("/*@cc_on return @_jscript_version; @*/")();

  if (jscriptVersion !== undefined) {
    isTheBrowser = true;
    actualVersion = jscriptMap[jscriptVersion];
  }

  ret = {
    isTheBrowser: isTheBrowser,
    actualVersion: actualVersion
  };

  return ret;
}());

const Detect = {
  versionSearchString: "",
  init: function () {
      const browser = this.searchString(this.dataBrowser),
        version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion),
        OS = this.searchString(this.dataOS);
      return {
        browser, 
        version, 
        OS,
        userAgent: navigator && navigator.userAgent,
        appVersion: navigator && navigator.appVersion,
        platform :navigator && navigator.platform
      };
  },
  searchString: function (data) {
      for (var i=0;i<data.length;i++) {
          var dataString = data[i].string;
          var dataProp = data[i].prop;
          this.versionSearchString = data[i].versionSearch || data[i].identity;
          if (dataString) {
              if (dataString.indexOf(data[i].subString) != -1)
                  return data[i].identity;
          }
          else if (dataProp)
              return data[i].identity;
      }
  },
  searchVersion: function (dataString) {
      var index = dataString.indexOf(this.versionSearchString);
      if (index == -1) return;
      return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
  },
  dataBrowser: [
      {
          string: navigator.userAgent,
          subString: "Chrome",
          identity: "Chrome"
      },
      { string: navigator.userAgent,
          subString: "OmniWeb",
          versionSearch: "OmniWeb/",
          identity: "OmniWeb"
      },
      {
          string: navigator.vendor,
          subString: "Apple",
          identity: "Safari",
          versionSearch: "Version"
      },
      {
        //@ts-ignore
          prop: window.opera,
          identity: "Opera",
          versionSearch: "Version"
      },
      {
          string: navigator.vendor,
          subString: "iCab",
          identity: "iCab"
      },
      {
          string: navigator.vendor,
          subString: "KDE",
          identity: "Konqueror"
      },
      {
          string: navigator.userAgent,
          subString: "Firefox",
          identity: "Firefox"
      },
      {
          string: navigator.vendor,
          subString: "Camino",
          identity: "Camino"
      },
      {
          /* For Newer Netscapes (6+) */
          string: navigator.userAgent,
          subString: "Netscape",
          identity: "Netscape"
      },
      {
          string: navigator.userAgent,
          subString: "MSIE",
          identity: "Internet Explorer",
          versionSearch: "MSIE"
      },
      {
          string: navigator.userAgent,
          subString: "Gecko",
          identity: "Mozilla",
          versionSearch: "rv"
      },
      {
          /* For Older Netscapes (4-) */
          string: navigator.userAgent,
          subString: "Mozilla",
          identity: "Netscape",
          versionSearch: "Mozilla"
      }
  ],
  dataOS: [
      {
          string: navigator.platform,
          subString: 'Win',
          identity: 'Windows'
      },
      {
          string: navigator.platform,
          subString: 'Mac',
          identity: 'macOS'
      },
      {
          string: navigator.userAgent,
          subString: 'iPhone',
          identity: 'iOS'
      },
      {
          string: navigator.userAgent,
          subString: 'iPad',
          identity: 'iOS'
      },
      {
          string: navigator.userAgent,
          subString: 'iPod',
          identity: 'iOS'
      },
      {
          string: navigator.userAgent,
          subString: 'Android',
          identity: 'Android'
      },
      {
          string: navigator.platform,
          subString: 'Linux',
          identity: 'Linux'
      }
  ]
};
export const sysDetails = Detect.init();
