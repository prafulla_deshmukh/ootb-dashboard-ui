export default class Pagination {
  data: object[];
  limit: number;
  offset: number;
  end: boolean;
  pages: object

  constructor(data, limit) {
    this.data = data;
    this.limit = limit;
    this.offset = 0;
    this.end = false;
    this.pages = {};
  }

  getPage(offset, limit) {
    return this.data.slice(offset, offset + limit);
  }

  next() {
    if(this.end) {
      return [];
    }

    const page = this.getPage(this.offset, this.limit);
    this.offset += this.limit;
    // console.log(page, this.offset);
    if(this.offset >= this.data.length) {
      this.end = true;
    }
    // this.pages["page-" + page] = page;
    return page;
  }

  hasNext() {
    return !this.end;
  }

  previous() {
    this.offset -= (this.limit * 2);
    if(this.offset < 0) {
      this.offset = 0;
      return [];
    }

    const page = this.getPage(this.offset, this.limit);

    this.offset += this.limit;
    this.end = false;
    // console.log("previous after fetch", this.offset);
    return page;
  }

  hasPrevious() {
    // console.log("hasPrevious", this.offset);
    return this.offset - this.limit > 0;
  }
};
