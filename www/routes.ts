import {routes as aboutRoutes} from "./modules/about";
import {LandingView} from "./modules/landing/LandingView";
import {routes as dashboardRoute} from "./modules/dashboard";
import {ErrorView} from "./modules/common/ErrorView";
import {routes as AuthRoutes} from "./modules/auth";

type Route = {
  path?: string;
  component: any;
  exact: boolean;
  name: string;
  auth: boolean;
}

// For Login Support
// Change auth parameter to true in specific route config

const routes: Route[] = [
  {
    path: "/",
    component: LandingView,
    exact: true,
    name: "Home",
    auth: false
  },
  // ...aboutRoutes,
  ...dashboardRoute,
  // ...AuthRoutes,
  {
    component: ErrorView,
    exact: true,
    name: "Page Not Found",
    auth:   false
  }
];

export default routes;
