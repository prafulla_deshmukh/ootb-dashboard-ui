import * as React from "react";
import Tab from "./Tab";

type ITabsProps = {
  change?: (index: number, label: string) => void,
  defaultTab?: number
}

type ITabsSates = {
  activeTab: number
};

class Tabs extends React.Component<ITabsProps, ITabsSates> {
  constructor(props) {
    super(props);

    this.state = {
      activeTab: this.props.defaultTab || 0
    };
  }

  componentWillReceiveProps = (nextProps) => {
    if(this.state.activeTab !== nextProps.defaultTab) {
      this.setState({
        activeTab: nextProps.defaultTab
      });
    }
  }

  onClickTabItem = (tab: number, label: string) => {
    this.setState({ activeTab: tab }, () => {
      this.props.change && this.props.change(tab, label);
    });
  }

  render() {
    const {
      onClickTabItem,
      props: {
        children,
      },
      state: {
        activeTab,
      }
    } = this;

    return (
      <div className="tabs">
        <ol className="tab-list">
          {
            //@ts-ignore
            children.map((child, i) => {
            const label= child.props["data-label"] || `Tab-${i}`;
            const title = child.props["data-title"] || "";
            return (
              <Tab
                activeTab={activeTab}
                index={i}
                key={`${i}-${label}`}
                label={label}
                onClick={onClickTabItem}
                title={title}
              />
            );
          })}
        </ol>
        <div className="tab-content">
          {
            //@ts-ignore
            children.map((child, i) => {
            if (i !== activeTab) return undefined;
            return child.props.children;
          })}
        </div>
      </div>
    );
  }
}

export default Tabs;
