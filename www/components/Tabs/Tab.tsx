import * as React from "react";

type ITabProps = {
  activeTab: number,
  index: number,
  label: string,
  title: string,
  onClick: Function
}

class Tab extends React.Component<ITabProps> {
  onClick = () => {
    const { index, onClick, label } = this.props;
    onClick(index, label);
  }

  render() {
    const {
      onClick,
      props: {
        activeTab,
        index,
        label,
        title
      },
    } = this;
    return (
      <li
        className={`tab-list-item ${(activeTab === index ? "tab-list-active" : "")}`}
        onClick={onClick} title={title}
      >
        {label}
      </li>
    );
  }
}

export default Tab;
