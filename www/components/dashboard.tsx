/* global console */

import * as React from "react"; // This import is required even if it isn't used

import { Layout } from "bru-web-sdk/packages/bru-ui-dashboard";
import { DataLayer } from "bru-web-sdk/packages/bru-ui-datalayer";


export default class Dashboard extends React.Component<any, any> {
  DataLayer: any;
  componentWillMount() {
    const { config } = this.props;
    this.initializeDashboard(config);
  }

  componentWillReceiveProps(nextProps) {
    const { config } = this.props;
    if (nextProps.config !== config) {
      if (this.DataLayer) {
        this.destroyChannels();
      }
      this.initializeDashboard(nextProps.config);
    }
    // const { config: { datalayer: datalayerConfig } } = this.props;
    // if (this.DataLayer) {
    //   this.destroyChannels();
    // }
    // this.initializeDashboard(nextProps.config);
  }

  initializeDashboard(config) {
    const { datalayer: datalayerConfig } = config;
    this.DataLayer = DataLayer(datalayerConfig.name);
    this.createChannels(datalayerConfig);
  }

  createChannels(datalayerConfig) {
    const { channels = [] } = datalayerConfig, DataLayer = this.DataLayer;
    channels.forEach(config => {
      DataLayer.createChannel(config);
    });
  }
  destroyChannels() {
    if (this.DataLayer) {
      this.DataLayer.destroyAllChannels();
    }
  }
  handleWidgetAction(id, actionName, data) {
    const { onWidgetAction } = this.props;
    return onWidgetAction && onWidgetAction(id, actionName, data);
  }
  render() {
    const { config: { style = {}, widgets: widgetConfig }, context } = this.props,
      widgets = widgetConfig.map(w => {
        let widget = Object.assign({}, w);
        widget.onAction = (act, data) => {
          return this.handleWidgetAction(w.id, act, data);
        };
        return widget;
      });

    return (
      <div className={`dashboard ${style.cssClass}`}>
        <Layout DataLayer={this.DataLayer} widgets={widgets} context={context} usePercentageWidth={true} horizontalOrder={false}></Layout>
      </div>
    );
  }
};
