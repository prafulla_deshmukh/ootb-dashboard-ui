const path = require("path");
const apiServer = require("./api/mock");
const Config = require("./www/config");
const webpack = require("webpack");
const fs = require("fs");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const theme = process.env.BRAND || "dark";

console.log(`Building app for ${theme} theme/brand`);

module.exports = {
  entry: ["@babel/polyfill", "./www/app.tsx"],
  output: {
    path: path.resolve(__dirname, "build"),
    filename: "bundle.js",
    publicPath: "/"
  },
  devtool: "source-map",
  module: {
    rules: [
      {
        test: /\.(t|j)sx?$/,
        use: "ts-loader",
        exclude: [/node_modules/]
      },
      {
        test: /\.(png|svg|jpe?g|jpg|gif|cur|woff(2)?|ttf|eot)(\?[a-z0-9]+)?$/i,
        use: [
          {
            loader: "url-loader",
            options: {
              limit: false,
              name: "[path][name].[ext]",
              outputPath: (url, resourcePath, context) => {
                return `${url.replace(/www\/|www\\/, "")}`;
              }
            }
          }
        ]
      },
      {
        test: /\.(s)?css$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: { importLoaders: 2, sourceMap: true }
          },
          {
            loader: "postcss-loader",
            options: {
              ident: "postcss",
              plugins: [require("autoprefixer")],
              sourceMap: true
            }
          },
          {
            loader: "resolve-url-loader",
            options: {
              removeCR: true,
              sourceMap: true
            }
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap: true,
              data: `@import "./www/theming/${theme}/index.scss";`,
              includePaths: [__dirname, "/www/theming"],
              sassOptions: {
                outputStyle: "compressed",
              }
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new CopyPlugin([
      {
        from: "shim_lib/*",
        to: path.resolve(__dirname, "build")
      }
    ]),
    new MiniCssExtractPlugin({
      filename: "style.css"
    }),
    new HtmlWebpackPlugin({
      template: "./www/index.html",
      favicon: "./www/images/favicon.png",
      title: Config.appname,
      minify: {
        trimCustomFragments: true,
        useShortDoctype: true,
        removeComments: true,
        collapseWhitespace: true,
        minifyJS: true,
        minifyCSS: true
      }
    }),
    new webpack.BannerPlugin(fs.readFileSync("./LICENSE", "utf8"))
  ],
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        uglifyOptions: {
          output: {
            comments: /Bruviti Confidential/i
          },
          compress: {
            drop_console: true
          }
        },
        sourceMap: true
      }),
      new OptimizeCSSAssetsPlugin({})
    ]
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js"]
  },
  devServer: {
    historyApiFallback: true,
    before(app) {
      apiServer.start(7001);
    },
    // host: "0.0.0.0",
    open: true
  }
};
