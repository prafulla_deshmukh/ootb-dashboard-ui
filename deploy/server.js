/*
 * Bruviti Confidential
 *
 * Copyright © 2019 Bruviti. All rights reserved.
 *
 * Notice: All information contained herein is, and remains the property of Bruviti
 * and its suppliers, if any. The intellectual and technical concepts
 * contained herein are proprietary to Bruviti and/or its suppliers, and may be
 * covered by U.S. and foreign patents or patents in process, and are protected
 * by trade secret or copyright law. Dissemination of this information or
 * reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from Bruviti.
 *
 */

const express = require("express"),
  http = require("http"),
  fs = require("fs"),
  webserver = express(),
  compression = require("compression"),
  port = 8080,
  API_URL = process.env.API_URL,
  BOT_URL = process.env.BOT_URL,
  VAD_URL = process.env.VAD_URL || "";

let configData = API_URL ? { apiServerUrl: API_URL } : {};
configData = Object.assign(
  configData,
  BOT_URL ? { botApi: { serverUrl: BOT_URL } } : {}
);

console.log("configData", configData);
console.log("API_URL", API_URL);
console.log("BOT_URL", BOT_URL);
console.log("VAD_URL", VAD_URL);

let updatedHTML = null,
  vadHTML = null,
  replaceEnvVarPromise = replaceEnvVar(`public/index.html`, [
    { placeHolder: `@configData@`, value: JSON.stringify(configData) }
  ]);
replaceVadLaunchPromise = replaceEnvVar(`vad-launch.html`, [
  { placeHolder: `@VAD_URL@`, value: VAD_URL },
  { placeHolder: `@API_URL@`, value: API_URL || "" }
]);

function replaceEnvVar(fileName, replaceDataArr) {
  console.log("fileName", fileName);

  return new Promise(function(resolve, reject) {
    fs.readFile(`${__dirname}/${fileName}`, "utf8", function(err, data) {
      if (err) {
        reject(err);
        return;
      }

      let fileHolder = data;
      replaceDataArr.forEach(function(rdata) {
        fileHolder = fileHolder.replace(rdata.placeHolder, rdata.value);
      });
      resolve(fileHolder);
    });
  });
}

replaceEnvVarPromise
  .then(data => {
    updatedHTML = data;
  })
  .catch(err => {
    console.log(err);
  });

replaceVadLaunchPromise
  .then(data => {
    vadHTML = data;
  })
  .catch(err => {
    console.log(err);
  });

webserver.use([compression(), express.static("public", { index: false })]);
const server = http.createServer(webserver);

server.listen(port, null, function() {
  console.log(
    "Express webserver configured and listening at http://localhost:" + port
  );
});

webserver.get(/^\//, (req, res, next) => {
  if (/^\/$/.test(req.url)) {
    res.redirect("/launch");
  } else {
    next();
  }
});

webserver.get("/launch", (req, res) => {
  if (!vadHTML) {
    replaceVadLaunchPromise
      .then(data => {
        vadHTML = data;
        res.send(vadHTML);
      })
      .catch(err => {
        console.log(err);
      });
  } else res.send(vadHTML);
});

webserver.get(/^(?!(\/launch)$).*$/i, (req, res) => {
  if (!updatedHTML) {
    replaceEnvVarPromise
      .then(data => {
        updatedHTML = data;
        res.send(updatedHTML);
      })
      .catch(err => {
        console.log(err);
      });
  } else res.send(updatedHTML);
});
