/* global require module console setInterval clearInterval */
const bodyParser = require("body-parser"),
    fs = require("fs"),
    jsonServer = require("json-server"),
    server = jsonServer.create(),
    router = jsonServer.router("./api/db.json"),
    db = router.db,
    middleware = jsonServer.defaults({static: "build"}),
    SseStream = require("ssestream");

// Set default middlewares (logger, static, cors and no-cache)
server.use(middleware);
server.use(bodyParser.urlencoded({extended: true}));
server.use(bodyParser.json());
/*
server.use(jsonServer.rewriter({
  "/users/:id/appliances": "/appliances?userId=:id",
  "/users/:id/appliances/:appId": "/appliances?id=:appId"
}));
*/

const anonPaths = {
  "/auth": 1,
  "/registration": 1
};

server.use("/api/", (req, res, next) => {
  const path = req.path;
  console.log("Path is", path);
  if(anonPaths[path]) {
    next();
  }else {
    // For Login Support
    /*
    const headers = req.headers, auth = headers.authorization;
    if(!auth) {
      res.status(401).json({errorMessage: "Not authorized"}).end();
      return;
    }
    const token = auth.split(/\s/)[1],
        userInfo = db.get("users").find({email: token}).value();
    if(!userInfo) {
      res.status(401).json({errorMessage: "Not authorized"}).end();
      return;
    }
    */

    // For get GUID as Authorization header

    const headers = req.headers, auth = headers.authorization;
    if(!auth) {
      res.status(401).json({errorMessage: "Not authorized"}).end();
      return;
    }

    next();
  }
});

server.post("/api/auth", (req, res) => {
  const {username, password} = req.body,
      user = db.get("users").find({email: username}).value();
  console.log(user);
  if(!user || user.password !== password) {
    res.status(401).json({errorMessage: "Invalid username or password"});
  }else {
    const userInfo = Object.assign({}, user, {token: `${username}`});
    res.json(userInfo);
  }
});

server.post("/api/feedback-service/stc/", (req, res) => {
  const {sessionId, code} = req.body,
      feedbackSTC = db.get("feedbackSTC").value();
  res.json(feedbackSTC[code]);
});

server.post("/api/feedback-service/answerfeedback/", (req, res) => {
  const {data} = req.body;
  res.json({
    message: "Successfully submitted"
  });
});

server.post("/api/logout", (req, res) => {
  res.json({
    message: "logout successfully"
  });
});

server.post("/api/log", (req, res) => {
  const {data} = req.body;
  res.json({
    message: "Log updated successfully"
  });
});

server.get("/api/test", (req, res) => {
  res.json({
    message: "Hello World"
  });
});

server.get("/api/appliance-info", (req, res) => {
  const {model, serial} = req.query,
      appliances = db.get("appliances").find({serial: serial}).value();

  if(appliances)
    res.json(appliances);
  else
    res.status(404).json({"message": "Appliance Not Found"});
});

server.get("/api/appliance-top-issues", (req, res) => {
  const {model} = req.query,
      topIssues = db.get("topIssues").find({model: model}).value();

  if(topIssues)
    res.json(topIssues);
  else
    res.status(404).json({"message": "Top Issues Not Found"});
  });

server.post("/api/appliance-document-search", (req, res) => {
  const {model, category, type} = req.body,
    records = db.get("resources").find({category: category}).value();

  if(!records)
    res.status(500).json({errorMessage: "Records not found"});

  res.json(records);
});
server.get("/api/appliance-service-history", (req, res) => {
  const {model, serial} = req.query;
  let {pageNumber, pageSize} = req.query;
  const issues = db.get("issueSerial").value();

  if(!pageNumber)
    pageNumber = 0
  if(!pageSize)
    pageSize = 5;

  const start = pageNumber * pageSize,
    end = start + pageSize,
    filterData = issues.slice(start, end);

  res.json({
    "content": filterData,
    "pageNumber": +pageNumber,
    "pageSize": +pageSize,
    "totalPages": +Math.ceil(issues.length/pageSize),
    "totalElements": +issues.length
  });
});

server.get("/api/appliance-stc-issues", (req, res) => {
  const {model, searchTerm} = req.query,
    records = db.get("issueSearchTerm").find({model: model}).value();;

  if(!model)
    res.status(500).json({errorMessage: "Model Number is required"});

  if(!records)
    res.status(500).json({errorMessage: "Records not found"});

  const latestIssues = records.issues.map(issue => {
      issue.stc_percentage = Math.ceil(Math.random() * 100);
      return issue;
    });

  res.json({
    model: model,
    issues: latestIssues
  });
});

server.get("/api/appliances/:id/notifications", (req, res) => {
  console.log("SSE client connected");
  const sse = new SseStream(req);
  sse.pipe(res);

  const id = setInterval(_ => {
    sse.write({
      data: {
        "heartbeat": 1,
        "test": Math.floor(Math.random() * 100)
      }
    });
    res.flush();
  }, 5000);

  res.on("close", _ => {
    console.log("SSE client disconnected");
    clearInterval(id);
    sse.unpipe(res);
  });
});

server.post("/api/feedback-service/contentfeedback/", (req, res) => {
  res.status(200).json({successMessage: "OK"});
});

module.exports = {
  start(port = 8000) {
    server.use("/api/", router);
    server.listen(port, _ => console.log("Started mock server on " + port));
  }
};
